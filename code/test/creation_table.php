<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	
	
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_valeurs_date = "SELECT `id_date` FROM table_date";
	$requete_valeurs_joueurs = "SELECT `id_joueur` FROM table_joueurs";
	$requete_valeurs_equipe = "SELECT `id_equipe` FROM table_equipes";
	//$requete_creation_ligne = "INSERT INTO `table_centrale` (`date`,`joueur`,`equipe_sdstt`) VALUES (?,?,?)";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	$nombre_date;
	$nombre_joueur;
	$nombre_equipe;
	//preparation
	$stmt_valeurs_date = mysqli_prepare($db,$requete_valeurs_date);
	$stmt_valeurs_joueurs = mysqli_prepare($db,$requete_valeurs_joueurs);
	$stmt_valeurs_equipe = mysqli_prepare($db,$requete_valeurs_equipe);
	if($stmt_valeurs_date)
	{
		//execution
		if(mysqli_stmt_execute($stmt_valeurs_date))	
		{
			mysqli_stmt_store_result($stmt_valeurs_date);
			$nombre_date = mysqli_stmt_num_rows($stmt_valeurs_date);
			if($stmt_valeurs_joueurs)
			{
			//execution
			if(mysqli_stmt_execute($stmt_valeurs_joueurs))	
				{
					mysqli_stmt_store_result($stmt_valeurs_joueurs);
					$nombre_joueur = mysqli_stmt_num_rows($stmt_valeurs_joueurs);
					if($stmt_valeurs_equipe)
					{
						//execution
						if(mysqli_stmt_execute($stmt_valeurs_equipe))	
						{
							mysqli_stmt_store_result($stmt_valeurs_equipe);
							$nombre_equipe = mysqli_stmt_num_rows($stmt_valeurs_equipe);
							//on possède les 3 valeurs pour les boucles
							/*if($stmt_creation_ligne)
							{*/
								$index_date=0;
								$index_joueur=0;
								$index_equipe=0;
								$index_adversaire=1;
								for($i=1;$i<8;$i++)	//nombre de journées
								{
								   for($k=1;$k<4;$k++)	//nombre d'équipe
								   {
									mysqli_query($db,"INSERT INTO `table_adversaire` (`id_adversaire_club_a`,`num_equipe`,`id_domext`) VALUES (1,1,1)");
									
									
									
								   for($j=1;$j<50;$j++)	//nombre de joueurs
									{
										mysqli_query($db,"INSERT INTO `table_centrale` (`date`,`joueur`,`equipe_sdstt`,`equipe_adversaire`,`etat_joueur_initial`,`etat_joueur_final`,`domext`,`rendez_vous`) VALUES (".$i.",".$j.",".$k.",1,1,1,1,1)");
										
									}
								   }
								}
									for($i=1;$i<8;$i++)
								{
								   for($k=1;$k<9;$k++)
								   {
								mysqli_query($db,"UPDATE `table_centrale` SET `equipe_adversaire`=".$index_adversaire."  WHERE (`date` =".$i." AND `equipe_sdstt`= ".$k." )");
								$index_adversaire++;
								}
								}
									/*	
										while($index_date<$nombre_date)
										{
											while($index_joueur<$nombre_joueur)
											{
												while($index_equipe<$nombre_equipe)
												{
													mysqli_stmt_bind_param($stmt_creation_ligne,'iii',$index_date,$index_joueur,$index_equipe);
													if(mysqli_stmt_execute($stmt_creation_ligne))
												$index_equipe++;	
												}
											$index_joueur++;
											}
										$index_date++;
										}*/
							/*}
							else
							{
							//code erreur de prepare
							$data['resultat'] = $msg['code_echec_05']['id'];
							}*/
						}
						else 	
						{	//échec de l'exécution
							$data['resultat'] = $msg['code_echec_01']['id'];
						}
					}
					else
					{
					//code erreur de prepare
					$data['resultat'] = $msg['code_echec_05']['id'];	
					}
				}
				else 	
				{	//échec de l'exécution
					$data['resultat'] = $msg['code_echec_01']['id'];
				}
			}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	mysqli_stmt_close($stmt_valeurs_date);
	mysqli_stmt_close($stmt_valeurs_joueurs);
	mysqli_stmt_close($stmt_valeurs_equipe);
	//mysqli_stmt_close($stmt_creation_ligne);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>