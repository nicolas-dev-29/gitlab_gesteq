	<!-- Modal ajout equipe-->
	<div class="modal fade"  id="modal_ajout_equipe" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_equipe" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'une équipe</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					 
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations de la nouvelle équipe</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div class="form-group">
							<label id="modal_ajout_equipe_nom_label" class="control-label" for "modal_ajout_equipe_nom">nom:</label> 
							<input id="modal_ajout_equipe_nom" type="text" class="form-control" placeholder="nom de l'équipe" autocomplete="off"   aria-describedby="modal_ajout_equipe_nom_aide" value="" required  >
							<span id="modal_ajout_equipe_nom_aide" class="help-block small">entrer le nom de l'équipe</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_equipe_erreur"> erreur!</span></div>
							<div class="form-group">
								<label id="modal_ajout_equipe_divers_label"for "modal_ajout_equipe_divers">divers:</label>
								<textarea  id="modal_ajout_equipe_divers" class="form-control"placeholder="renseignements divers sur l'équipe" rows="3" aria-describedby="modal_ajout_equipe_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_ajout_equipe_divers_erreur"> erreur!</span></div>
							</div>
						</div>
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_equipe_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>