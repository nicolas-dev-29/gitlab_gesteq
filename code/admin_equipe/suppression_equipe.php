<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation des requêtes
	$requete_suppression = "DELETE FROM table_equipes
	WHERE `id_equipe`=?";
	$requete_update_table_principale= "DELETE FROM `table_centrale`
	WHERE `equipe_sdstt`=?";
	$requete_liste_joueurs_en_equipe = "SELECT DISTINCT `joueur` FROM `table_centrale` WHERE( equipe_sdstt=? AND etat_joueur_final=10)";
	$requete_update_joueur  =  "UPDATE `table_centrale`SET `etat_joueur_final`=? WHERE (joueur=?)";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//preparation des requetes
	$stmt_suppression = mysqli_prepare($db,$requete_suppression);
	$stmt_update_table_principale = mysqli_prepare($db, $requete_update_table_principale);
	$stmt_update_joueur = mysqli_prepare($db, $requete_update_joueur);
	$stmt_liste_joueurs_en_equipe = mysqli_prepare($db, $requete_liste_joueurs_en_equipe);
	/*if($stmt_verification)
	{*/
		if(isset($_POST['id_equipe_sdstt']) && $_POST['id_equipe_sdstt']!="")
		{
			//nettoyage des informations provenant de POST
			if((filter_input(INPUT_POST,'id_equipe_sdstt',FILTER_SANITIZE_NUMBER_INT)==FALSE))
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_equipe_base=filter_input(INPUT_POST,'id_equipe_sdstt',FILTER_SANITIZE_NUMBER_INT);
				if(mysqli_stmt_bind_param($stmt_liste_joueurs_en_equipe,'i',$id_equipe_base))
				{
				if(mysqli_stmt_execute($stmt_liste_joueurs_en_equipe))
					{
						mysqli_stmt_store_result($stmt_liste_joueurs_en_equipe);
						mysqli_stmt_bind_result($stmt_liste_joueurs_en_equipe,$ligne['liste_joueur']);
						while(mysqli_stmt_fetch($stmt_liste_joueurs_en_equipe))
						{
							$res_liste_joueur = $ligne['liste_joueur'];
							if(mysqli_stmt_bind_param($stmt_update_joueur,'ii',$defaut,$res_liste_joueur))
							{
								if(mysqli_stmt_execute($stmt_update_joueur))
								{
								$data['resultat'] = $msg['code_ok']['id'];	
								}
								else
								{
								$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						}	
					}
					else
					{
					$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
				//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
				}
				if(mysqli_stmt_bind_param($stmt_suppression,'i',$id_equipe_base))
				{
					if(mysqli_stmt_execute($stmt_suppression))
					{
						mysqli_stmt_store_result($stmt_suppression);
						$nbre = mysqli_stmt_num_rows($stmt_suppression);
							if(mysqli_stmt_bind_param($stmt_update_table_principale,'i',$id_equipe_base))
							{
								if(mysqli_execute($stmt_update_table_principale))
								{		
									$data['resultat'] = $msg['code_ok']['id'];	
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
/*	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}
*/
	mysqli_stmt_close($stmt_suppression);
	mysqli_stmt_close($stmt_update_table_principale);
	//encodage JSON
	header('Content-Type: application/json');
	echo json_encode($data);	
	mysqli_close($db);	
?>