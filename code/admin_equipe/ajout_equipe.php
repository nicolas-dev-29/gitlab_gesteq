<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_valeurs_date = "SELECT `id_date` FROM table_date";
	$requete_valeurs_etat_initial = "SELECT distinct  `etat_joueur_initial`,`etat_joueur_final` FROM `table_centrale` WHERE (  `date`=? AND `joueur`=?)  ";
	$requete_valeurs_equipe = "SELECT `id_equipe` FROM table_equipes WHERE `nom_equipe` = ?";
	$requete_ajout_equipe ="INSERT INTO `table_equipes`( `nom_equipe`, `divers_equipe`) VALUES (?,?)";
	$requete_update_table_centrale = "INSERT INTO `table_centrale`( `date`, `joueur`, `championnat`, `niveau_championnat`, `poule`, `etat_joueur_initial`, `etat_joueur_final`, `equipe_sdstt`, `equipe_adversaire`, `domext`, `rendez_vous`) 
	VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	//$requete_creation_ligne = "INSERT INTO `table_centrale` (`date`,`joueur`,`equipe_sdstt`) VALUES (?,?,?)";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	$nombre_date;
	$nombre_joueur;
	$nombre_equipe;
	//preparation
	$stmt_valeurs_date = mysqli_prepare($db,$requete_valeurs_date);
	$stmt_valeurs_equipe = mysqli_prepare($db,$requete_valeurs_equipe);
	$stmt_ajout_equipe = mysqli_prepare($db,$requete_ajout_equipe);
	$stmt_valeurs_etat_initial = mysqli_prepare($db,$requete_valeurs_etat_initial);
	$stmt_update_table_centrale = mysqli_prepare($db,$requete_update_table_centrale);
	if($stmt_ajout_equipe)
	{
		if(isset($_POST['nom']) && $_POST['nom']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'nom',FILTER_SANITIZE_STRING)==FALSE)
			{	//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$nom_base =filter_input(INPUT_POST,'nom',FILTER_SANITIZE_STRING);
				$divers_base = filter_input(INPUT_POST,'divers',FILTER_SANITIZE_STRING);
				mysqli_stmt_bind_param($stmt_ajout_equipe,'ss',$nom_base,$divers_base);
							//execution
							if(mysqli_stmt_execute($stmt_ajout_equipe))	
							{
								mysqli_stmt_store_result($stmt_ajout_equipe);
								$nombre = mysqli_stmt_num_rows($stmt_ajout_equipe);
								//récupération de l'id insérer
								if($stmt_valeurs_equipe)
								{
									mysqli_stmt_bind_param($stmt_valeurs_equipe,'s',$nom_base);
									if(mysqli_stmt_execute($stmt_valeurs_equipe))
									{
										mysqli_stmt_bind_result($stmt_valeurs_equipe,$ligne['id_equipe']);
										$id_equipe_base=0;
										while(mysqli_stmt_fetch($stmt_valeurs_equipe))
										{
											$id_equipe_base = $ligne['id_equipe'];
										}	
										//mise à jour table centrale
										if($stmt_update_table_centrale)
										{
											for($i=1;$i<$nbre_max_date+1;$i++) //date
											{
											   for($j=1;$j<$nbre_max_joueurs+1;$j++)	//joueur
											   {
													if($stmt_valeurs_etat_initial)
													{
														mysqli_stmt_bind_param($stmt_valeurs_etat_initial,'ii',$i,$j);
														if(mysqli_stmt_execute($stmt_valeurs_etat_initial))	
														{
															mysqli_stmt_bind_result($stmt_valeurs_etat_initial,$ligne['etat_initial'],$ligne['etat_final']);
															while(mysqli_stmt_fetch($stmt_valeurs_etat_initial))
															{
																$id_etat_initial = $ligne['etat_initial'];
																if($ligne['etat_final']>9) //le joueur est déjà inclus dans une équipe
																{
																	$id_etat_final = 11;
																}
																else
																{
																
																	$id_etat_final=1;
																}																	
																
															}
															mysqli_stmt_bind_param($stmt_update_table_centrale,'iiiiiiiiiii',$i,$j,$defaut,$defaut,$defaut,$id_etat_initial,$id_etat_final,$id_equipe_base,$defaut,$defaut,$defaut);
															//execution
															if(mysqli_stmt_execute($stmt_update_table_centrale))	
															{
																$data['resultat'] = $msg['code_ok']['id'];		
															}
															else
															{
																//erreur d'execute
																$data['resultat'] = $msg['code_echec_01']['id'];	
															}
														}
														else
														{
														//erreur d'execute
														$data['resultat'] = $msg['code_echec_01']['id'];
														}
													}
													else
													{
													//code erreur de prepare
													$data['resultat'] = $msg['code_echec_05']['id'];
													}
												}
											}
										}
								else
								{
								//code erreur de prepare
								$data['resultat'] = $msg['code_echec_05']['id'];	
								}
									}
									else
									{
									//échec de l'exécution
									$data['resultat'] = $msg['code_echec_01']['id'];
									}
								}
								else
								{
								//code erreur de prepare
								$data['resultat'] = $msg['code_echec_05']['id'];	
								}
							}
							else 	
							{	//échec de l'exécution
								$data['resultat'] = $msg['code_echec_01']['id'];
							}
			}
		}
		else
		{
		$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	mysqli_stmt_close($stmt_ajout_equipe);
	mysqli_stmt_close($stmt_update_table_centrale);
	
	mysqli_stmt_close($stmt_valeurs_date);
	mysqli_stmt_close($stmt_valeurs_equipe);
	mysqli_stmt_close($stmt_valeurs_etat_initial);

	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>