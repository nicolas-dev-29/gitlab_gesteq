<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//mail
	require '../../fonctions/fonction_mail.inc';
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
		//décodage de la table provenant du lient en JSON
	$tab_etats= array();
	$nbre_erreur=0;
		//prépararion d'envoi des mails
	//nouvelle fonction	
	//$expediteur = 'convocations.sdstt@gmail.com';
	//$alias_expediteur ='tennis de table - championnat jeunes';	
	$data_mail_adresse=array();
	//sujet
	$data_mail_sujet=" modifications disponibilités - récapitulatif";	
	$texte_info_mail_entete="Voici les nouvelles disponibilités du joueur : <br>";
	$data_mail_corps_alt="";
	$texte_info_mail="";	
	$sujet_info_mail_headcoach="";
	$adresses_mail_joueurs="";
	/*$entetes = '';
		// en-têtes expéditeur
		$entetes .="From:".$from_adresse_locale_hebergeur."\r\n";
		// en-têtes adresse de retour
		$entetes .= "Reply-To:".$replyto_adresse_locale_hebergeur. "\r\n";
		// personnes en copie
		$entetes .= "Cc : pascalhallegouet@gmail.com"."\r\n";
		$entetes .= "Bcc : sdstt29@gmail.com"."\r\n";
		// type de contenu HTML
		$entetes .= "Content-type: text/html; \r\n";*/
	//faire un isset sur tab
	$tab_etats = json_decode($_POST['tab'],true);
	//préparation des requêtes
	$requete_update = "UPDATE `table_centrale` SET 
	`etat_joueur_initial`=?,`etat_joueur_final`=? 
	WHERE (`date`=? AND`joueur`= ?)";
	$requete_info_mail = "SELECT DISTINCT `joueur`,`nom_date`,`nom_joueur`,`prenom_joueur`,`nom_etat_joueur`,`mail_1_joueur`,`mail_2_joueur`
	FROM `table_centrale` TC
	INNER JOIN table_joueurs TJ
		ON  TC.joueur = TJ.id_joueur
	INNER JOIN table_date TD
		ON  TC.date = TD.id_date
   	INNER JOIN table_etats_joueurs_init TEJI
		ON  TC.etat_joueur_initial = TEJI.id_etat_joueur
	WHERE (`joueur`= ? )
	";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//preparation des requetes
	$stmt_update = mysqli_prepare($db, $requete_update);
	$stmt_info_mail = mysqli_prepare($db, $requete_info_mail);
		if(isset($_POST['id_joueur_selectionne']) && $_POST['id_joueur_selectionne']!="" )
		{
			//nettoyage des informations provenant de POST
			if((filter_input(INPUT_POST,'id_joueur_selectionne',FILTER_SANITIZE_NUMBER_INT)==FALSE))
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides	
			{
				$id_joueur_base=filter_input(INPUT_POST,'id_joueur_selectionne',FILTER_SANITIZE_NUMBER_INT);
				$id_etat_final_base = 1;
				foreach($tab_etats as $index => $ligne_etats)
				{
							$ligne_index = $index+1;
							if(mysqli_stmt_bind_param($stmt_update,'iiii',$ligne_etats,$id_etat_final_base,($ligne_index),$id_joueur_base))
							{
								if(mysqli_execute($stmt_update))
								{		
									$data['resultat'] = $msg['code_ok']['id'];	
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
									$nbre_erreur++;
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							$nbre_erreur++;
							}
				}
				if(mysqli_stmt_bind_param($stmt_info_mail,'i',$id_joueur_base))
							{
								if(mysqli_execute($stmt_info_mail))
								{		
									$nbre = mysqli_stmt_affected_rows($stmt_info_mail);
									mysqli_stmt_bind_result($stmt_info_mail,$ligne['joueur'],$ligne['nom_date'],$ligne['nom_joueur'],$ligne['prenom_joueur'],$ligne['nom_etat_joueur'],$ligne['mail_1_joueur'],$ligne['mail_2_joueur']);
									//$texte_info_mail="Voici les disponibilités du joueur ";
									$modifications="";
									while(mysqli_stmt_fetch($stmt_info_mail))
									{
									//$sujet_info_mail_headcoach=$ligne['nom_joueur']."-".$ligne['prenom_joueur']."- mise à jour disponibilités";
									$nom_prenom = $ligne['nom_joueur']." - ".$ligne['prenom_joueur']."<br>";
									$modifications.="le ".$ligne['nom_date'].", le joueur est -->".$ligne['nom_etat_joueur']."<br>";
										if($ligne['mail_1_joueur'] !='')
											{
												array_push($data_mail_adresse,$ligne['mail_1_joueur']);
											}
											if($ligne['mail_2_joueur'] !='')
											{
												array_push($data_mail_adresse,$ligne['mail_2_joueur']);
											}
									}
									$texte_info_mail.=$texte_info_mail_entete.$nom_prenom."<br>".$modifications;
									$data['resultat'] = $msg['code_ok']['id'];	
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
									$nbre_erreur++;
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							$nbre_erreur++;
							}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
			$nbre_erreur++;
		}
	//vérification des erreurs
		if($nbre_erreur == 0) //tout est ok
		{
			$data['resultat'] = $msg['code_ok']['id'];
			//envoi mail vers le joueurs (mail parent1 et parent2)
			//$data_mail_adresse = $adresses_mail_joueurs;
			$data['adresses']=$data_mail_adresse;
			//$data['entetes'] = $entetes;
			//$data['sujet'] = $sujet_info_mail_joueur;
			$pied_de_page = "<br> cordialement <br> les entraineurs jeunes";
			$data['mail'] = $texte_info_mail.$pied_de_page;
			//$res_mail = mail($data_mail_adresse,$sujet_info_mail_joueur,$texte_info_mail,$entetes);
			$res_mail = envoi_mail_multiple($expediteur,$alias_expediteur,$data_mail_adresse,$adresses_copies,$data_mail_sujet,$data['mail'],$data_mail_corps_alt);
			$data['res_mail']= $res_mail;
			
			//envoi mail head_coach
			//$data_mail_adresse = $adresse_head_coach;
			//$res_mail = mail($data_mail_adresse,$sujet_info_mail_headcoach,$texte_info_mail,$entetes);
			//$data['res mail_2'] = $res_mail;
		}
	//femeture
	mysqli_stmt_close($stmt_update);
	mysqli_stmt_close($stmt_info_mail);
	//encodage JSON
	header('Content-Type: application/json');
	echo json_encode($data);	
	mysqli_close($db);	
?>