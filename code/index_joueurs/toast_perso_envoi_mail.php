<!------------------------------------------------------------------------------------->
<!-- code pour les toasts de validation d'enregistrement et d'échec d'enregistrement -->
<!------------------------------------------------------------------------------------->





<!-- toast validation -->
	
		<div class="toast mytoast_validation " id="toast_mail_ok" 
		data-delay="5000"  
		role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
				<strong class="mr-auto">Enregistrements réussis</strong>
				<small></small>
				<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Fermer"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="toast-body">
				<span id="toast_mail_ok_texte">OK</span>
			</div>
		</div>
	
	
	<!-- toast echec -->
	
		<div class="toast mytoast_echec " id="toast_mail_echec" 
		data-delay="5000"  
		role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
				<strong class="mr-auto">erreur, veuillez réessayer</strong>
				<small></small>
				<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Fermer"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="toast-body">
				<span id="toast_mail_echec_texte">Ko</span>
			</div>
		</div>
	