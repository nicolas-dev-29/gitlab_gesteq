<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_valeurs = "SELECT DISTINCT `joueur`,`date`,`etat_joueur_initial` ,`nom_etat_joueur`
	FROM `table_centrale`  TC
	JOIN table_etats_joueurs_init TEJI
		ON TEJI.`id_etat_joueur` = TC.`etat_joueur_initial`
	WHERE `joueur` = ?
	ORDER BY `date`";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	$tab_joueur = array();
	$tab_date=array();
	$tab_etat=array();
	
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	
	//preparation
	$stmt_valeurs = mysqli_prepare($db,$requete_valeurs);
	if($stmt_valeurs)
	{
	if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{	
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	
			{
			//  les données sont valides
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				//acquisition parametre logistique
				if(mysqli_stmt_bind_param($stmt_valeurs,'i',$id_base))
				{
					//execution
					if(mysqli_stmt_execute($stmt_valeurs))	
					{
						$nbre = mysqli_stmt_affected_rows($stmt_valeurs);
						mysqli_stmt_bind_result($stmt_valeurs,$ligne['joueur'],$ligne['date'],$ligne['etat_joueur_initial'],$ligne['nom_etat_joueur']);
						$index=0;
						while(mysqli_stmt_fetch($stmt_valeurs))
						{
							$data[$index]['joueur'] = $ligne['joueur'];
							$data[$index]['date'] = $ligne['date'];
							$data[$index]['etat'] = $ligne['etat_joueur_initial'];
							$data[$index]['nom_etat'] = $ligne['nom_etat_joueur'];
							$index++;
						}		

					}
					else 	
					{	//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}	
			}
		}
		else
		{
			//erreur de POST
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	mysqli_stmt_close($stmt_valeurs);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>