<?php
	header( 'content-type: text/html; charset=utf-8' );	
	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation des requêtes
	
	$requete_update = "UPDATE `table_centrale` SET 
	`etat_joueur_initial`=?,`etat_joueur_final`=? 
	WHERE (`date`=? AND`joueur`= ?)";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	
	$data=array();
	
	//preparation des requetes
	//$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_update = mysqli_prepare($db, $requete_update);
	
	/*if($stmt_verification)
	{*/
		if(isset($_POST['id_date_selectionne']) && $_POST['id_date_selectionne']!="" && isset($_POST['id_joueur_selectionne']) && $_POST['id_joueur_selectionne']!="" && isset($_POST['id_etat_initial_selectionne']) && $_POST['id_etat_initial_selectionne']!="")
		{
			//nettoyage des informations provenant de POST
			if((filter_input(INPUT_POST,'id_date_selectionne',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_joueur_selectionne',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_etat_initial_selectionne',FILTER_SANITIZE_NUMBER_INT)==FALSE))
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_date_base=filter_input(INPUT_POST,'id_date_selectionne',FILTER_SANITIZE_NUMBER_INT);
				$id_joueur_base=filter_input(INPUT_POST,'id_joueur_selectionne',FILTER_SANITIZE_NUMBER_INT);
				$id_etat_initial_base=filter_input(INPUT_POST,'id_etat_initial_selectionne',FILTER_SANITIZE_NUMBER_INT);
				if($id_etat_initial_base == $id_etat_joueur_joue)
				{
				$id_etat_final_base = $id_etat_joueur_occupe;
				}
				else
				{
				$id_etat_final_base = 1;
				}
				 
				/*
				if(mysqli_stmt_bind_param($stmt_verification,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_verification))
					{
						mysqli_stmt_store_result($stmt_verification);
						$nbre = mysqli_stmt_num_rows($stmt_verification);
						if($nbre == 1)	// l'id est unique et est trouvé
						{*/
							if(mysqli_stmt_bind_param($stmt_update,'iiii',$id_etat_initial_base,$id_etat_final_base,$id_date_base,$id_joueur_base))
							{
								if(mysqli_execute($stmt_update))
								{		
									$data['resultat'] = $msg['code_ok']['id'];;	
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						/*}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}*/
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
/*	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}
*/

	
	
								

		

	mysqli_stmt_close($stmt_update);


	
	
	//encodage JSON
	header('Content-Type: application/json');
	echo json_encode($data);	
	mysqli_close($db);	
?>