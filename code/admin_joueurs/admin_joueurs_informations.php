<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	$requete_liste = "SELECT `id_joueur`,`nom_joueur`,`prenom_joueur`,`classement`,`selection`,`mail_1_joueur`,`tel_1_joueur`,`mail_2_joueur`,`tel_2_joueur` ,`divers_joueur`
                        FROM table_joueurs WHERE `id_joueur` = ?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
	    if(isset($_POST['id']) && $_POST['id']!="")
	    {

    		    //nettoyage des informations provenant de POST
    		    if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
    		    {
    		        //erreur de typage
    		        $data['resultat']=$msg['code_echec_04']['id'];
    		    }
    		    else	//  les données sont valides
    		    {
    		        $id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
    		        		        
    		        if(mysqli_stmt_bind_param($stmt_liste,'i',$id_base))
    		        {
    		            if(mysqli_stmt_execute($stmt_liste))
    		            {
    		                mysqli_stmt_store_result($stmt_liste);
    		                $nbre = mysqli_stmt_num_rows($stmt_liste);
    		                if($nbre == 1)	// l'id est unique et est trouvé
    		                {
    		                  mysqli_stmt_bind_result($stmt_liste,$ligne['id_joueur'],$ligne['nom_joueur'],$ligne['prenom_joueur'],$ligne['classement'],$ligne['selection'],$ligne['mail_parent_1']
        			                 ,$ligne['tel_parent_1'],$ligne['mail_parent_2'],$ligne['tel_parent_2'],$ligne['divers']);
        			         
        			          while(mysqli_stmt_fetch($stmt_liste))
        			          {
        			              $data['resultat'] = $msg['code_ok']['id'];
        			              $data['id'] = htmlentities($ligne['id_joueur'],ENT_QUOTES,'UTF-8');
        			              $data['nom'] = htmlentities($ligne['nom_joueur'],ENT_QUOTES,'UTF-8');
        			              $data['prenom'] = htmlentities($ligne['prenom_joueur'],ENT_QUOTES,'UTF-8');
        			              $data['classement'] = htmlentities($ligne['classement'],ENT_QUOTES,'UTF-8');
        			              $data['selection'] = htmlentities($ligne['selection'],ENT_QUOTES,'UTF-8');
        			              $data['mail_parent_1'] = htmlentities($ligne['mail_parent_1'],ENT_QUOTES,'UTF-8');
        			              $data['tel_parent_1'] = htmlentities($ligne['tel_parent_1'],ENT_QUOTES,'UTF-8');
        			              $data['mail_parent_2'] = htmlentities($ligne['mail_parent_2'],ENT_QUOTES,'UTF-8');
        			              $data['tel_parent_2'] = htmlentities($ligne['tel_parent_2'],ENT_QUOTES,'UTF-8');
        			              $data['divers'] = htmlentities($ligne['divers'],ENT_QUOTES,'UTF-8');
        			             
        			          }
    		                }
    		                else
    		                {
    		                    $data['resultat'] = $msg['code_echec_04']['id'];
    		                }
    		            }
    		            else
    		            {
    		                $data['resultat'] = $msg['code_echec_03']['id'];
    		            }
    		        
    		        }
    		        else
    		        {
    		            $data['resultat'] = $msg['code_echec_03']['id'];
    		        }
    		    }

	    }
		else 	
		{
			$data['resultat'] = $msg['code_echec_02']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
			
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>