	 
	<!-- Modal ajout joueurs-->
	<div class="modal fade"  id="modal_ajout_joueurs" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_joueurs" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'un nouveau joueur / nouvelle joueuse</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					 
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouveau joueur / nouvelle joueuse</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div class="form-group">
							<span id="modal_ajout_joueurs_id" value="" ></span> <!-- le span n'est pas visible, il sert à stocker l'id du lieu pour faire la requete AJAX directement avec l'id au lieu du nom-->
							
							<label id="modal_ajout_joueurs_nom_label" class="control-label" for "modal_ajout_joueurs_nom">nom:</label> 
							<input id="modal_ajout_joueurs_nom" type="text" class="form-control" placeholder="nom" autocomplete="off"   aria-describedby="modal_ajout_joueurs_nom_aide" value="" required  >
							
							<label id="modal_ajout_joueurs_prenom_label" class="control-label" for "modal_ajout_joueurs_prenom">prenom:</label> 
							<input id="modal_ajout_joueurs_prenom" type="text" class="form-control" placeholder="prenom" autocomplete="off"   aria-describedby="modal_ajout_joueurs_prenom_aide" value="" required  >
							
							<label id="modal_ajout_joueurs_classement_label" class="control-label" for "modal_ajout_joueurs_classement">classement:</label> 
							<input id="modal_ajout_joueurs_classement" type="text" class="form-control" placeholder="classement" autocomplete="off"   aria-describedby="modal_ajout_joueurs_classement_aide" value="" required  >

							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_joueurs_erreur"> erreur!</span></div>
							<div class="form-group">
								<label id="modal_ajout_joueurs_divers_label"for "modal_ajout_joueurs_divers">divers:</label>
								<textarea  id="modal_ajout_joueurs_divers" class="form-control"placeholder="renseignements divers " rows="3" aria-describedby="modal_ajout_joueurs_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_ajout_joueurs_divers_erreur"> erreur!</span></div>
							</div>
						</div>
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_joueurs_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal modification joueurs-->
	<div class="modal fade"  id="modal_modification_joueurs" tabindex="-1" role="dialog" aria-labelledby="modal_modification_joueurs" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_modification">
					<h5 class="modal-title">modification d'un nouveau joueur / nouvelle joueuse</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					 
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouveau joueur / nouvelle joueuse</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div class="form-group">
							<span id="modal_modification_joueurs_id" value="" ></span> <!-- le span n'est pas visible, il sert à stocker l'id du lieu pour faire la requete AJAX directement avec l'id au lieu du nom-->
							
							<label id="modal_modification_joueurs_nom_label" class="control-label" for "modal_modification_joueurs_nom">nom:</label> 
							<input id="modal_modification_joueurs_nom" type="text" class="form-control" placeholder="nom" autocomplete="off"   aria-describedby="modal_modification_joueurs_nom_aide" value="" required  >
							<!-- <div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_joueurs_erreur"> erreur!</span></div>	 -->						
							<label id="modal_modification_joueurs_prenom_label" class="control-label" for "modal_modification_joueurs_prenom">prenom:</label> 
							<input id="modal_modification_joueurs_prenom" type="text" class="form-control" placeholder="prenom" autocomplete="off"   aria-describedby="modal_modification_joueurs_prenom_aide" value="" required  >
							<div class="row">
    							<div class="col-lg-6">
    								<h5>parent 1:</h5>
    								<label id="modal_modification_joueurs_mail_1_label" class="control-label" for "modal_modification_joueurs_nom">mail 1:</label> 
									<input id="modal_modification_joueurs_mail_1" type="email" class="form-control" placeholder="mail parent 1" value=""  required >			
    	
    								<label id="modal_modification_joueurs_tel_1_label" class="control-label" for "modal_modification_joueurs_nom">téléphone 1:</label> 
									<input id="modal_modification_joueurs_tel_1" type="text" class="form-control" placeholder="téléphone parent 1" autocomplete="off"   aria-describedby="modal_modification_joueurs_nom_aide" value=""  >			

    							</div>
    							<div class="col-lg-6">
    							<h5>parent 2:</h5>
    								<label id="modal_modification_joueurs_mail_2_label" class="control-label" for "modal_modification_joueurs_nom">mail 2:</label> 
									<input id="modal_modification_joueurs_mail_2" type="email" class="form-control" placeholder="mail parent 2" autocomplete="off"   aria-describedby="modal_modification_joueurs_nom_aide" value=""   >			
    	
    								<label id="modal_modification_joueurs_tel_2_label" class="control-label" for "modal_modification_joueurs_nom">téléphone 2:</label> 
									<input id="modal_modification_joueurs_tel_2" type="text" class="form-control" placeholder="téléphone parent 2" autocomplete="off"   aria-describedby="modal_modification_joueurs_nom_aide" value=""  >			
    							
    							
    							</div>
							</div>
							
							
							<label id="modal_modification_joueurs_classement_label" class="control-label" for "modal_modification_joueurs_classement">classement:</label> 
							<input id="modal_modification_joueurs_classement" type="text" class="form-control" placeholder="classement" autocomplete="off"   aria-describedby="modal_modification_joueurs_classement_aide" value="" required  >


							<div class="form-group">
								<label id="modal_modification_joueurs_divers_label"for "modal_modification_joueurs_divers">divers:</label>
								<textarea  id="modal_modification_joueurs_divers" class="form-control"placeholder="renseignements divers " rows="3" aria-describedby="modal_modification_joueurs_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_modification_joueurs_divers_erreur"> erreur!</span></div>
							</div>
						</div>
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_modification_joueurs_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal suppression d'un lieu-->
	<div class="modal fade" id="modal_suppression_joueurs" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_joueurs" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
			
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression d'un lieu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer le lieu suivant:</p>	
					<span id="modal_suppression_joueurs_id" value="" ></span> <!-- le span n'est pas visible, il sert à stocker l'id du lieu pour faire la requete AJAX directement avec l'id au lieu du nom-->
					<div class="texte_important">nom: </div><span id="modal_suppression_joueurs_nom" value="">nom</span> </br> 
					<div class="texte_important">divers: </div>	
					<div class="modal_span">
						<span id="modal_suppression_joueurs_divers"value=""></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_joueurs_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>

