<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_liste = "SELECT DISTINCT `id_joueur`,`nom_joueur`,`prenom_joueur`,`classement` 
	FROM `table_joueurs` 
	WHERE NOT(`prenom_joueur`='')
	WHERE `nom_joueur`= ? ";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_STRING)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
				{
				$nom_base='"'.$_POST['id'].'"';//filter_input(INPUT_POST,'id',FILTER_SANITIZE_STRING);					
				if(mysqli_stmt_bind_param($stmt_liste,'s',$nom_base))
				{
				//execution
					if(mysqli_stmt_execute($stmt_liste))	
					{
						$nbre = mysqli_stmt_affected_rows($stmt_liste);
						mysqli_stmt_bind_result($stmt_liste,$ligne['id_joueur'],$ligne['nom_joueur'],$ligne['prenom_joueur'],$ligne['classement']);
						$index=0;
						while(mysqli_stmt_fetch($stmt_liste))
						{
							$data[$index]['resultat'] = $msg['code_ok']['id'];
							$data[$index]['id_joueur'] = $ligne['id_joueur'];
							$data[$index]['nom_joueur'] = $ligne['nom_joueur'];
							$data[$index]['prenom_joueur'] = $ligne['prenom_joueur'];
							$data[$index]['classement'] = $ligne['classement'];
							$index++;
						}		
					}
					else 	
					{	//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
				//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
				}
				}
		}
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>