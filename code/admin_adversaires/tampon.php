<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation des requêtes
	$requete_select_adversaire = "SELECT `equipe_adversaire` 
	FROM `table_centrale`
	WHERE (`date`=? AND `equipe_sdstt`=?)";
	
	$requete_update_1 = "UPDATE `table_adversaire` SET 
	`id_adversaire_club_a`=? , `num_equipe`=?
	WHERE (`id_adversaire`=?)"; 
	
	$requete_select = "SELECT id_adversaire
	FROM `table_adversaire`
	WHERE (`id_adversaire_club_a`=? AND `num_equipe`=?)
	";
	$requete_update_2 = "UPDATE `table_centrale` SET 
	`equipe_adversaire`=? , `domext` = ?
	WHERE (`date`=?  AND `equipe_sdstt`=?)";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	$nbre = 0;
	$id_adversaire_update = 0;
	//preparation des requetes
	//$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_update_1 = mysqli_prepare($db, $requete_update_1);
	$stmt_update_2 = mysqli_prepare($db, $requete_update_2);
	$stmt_select = mysqli_prepare($db, $requete_select);
	$stmt_select_adversaire = mysqli_prepare($db, $requete_select_adversaire);
	/*if($stmt_verification)
	{*/
		if(isset($_POST['id_date']) && $_POST['id_date']!="" && isset($_POST['id_equipe']) && $_POST['id_equipe']!="" && isset($_POST['id_adversaire']) && $_POST['id_adversaire']!="" && isset($_POST['id_num_equipe']) && $_POST['id_num_equipe']!=""&& isset($_POST['id_dom_ext']) && $_POST['id_dom_ext']!="")
		{
			//nettoyage des informations provenant de POST
			if((filter_input(INPUT_POST,'id_date',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_equipe',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_dom_ext',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_num_equipe',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_adversaire',FILTER_SANITIZE_NUMBER_INT)==FALSE))
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_date_base=filter_input(INPUT_POST,'id_date',FILTER_SANITIZE_NUMBER_INT);
				$id_equipe_base=filter_input(INPUT_POST,'id_equipe',FILTER_SANITIZE_NUMBER_INT);
				$id_adversaire_base=filter_input(INPUT_POST,'id_adversaire',FILTER_SANITIZE_NUMBER_INT);
				$id_num_equipe_base=filter_input(INPUT_POST,'id_num_equipe',FILTER_SANITIZE_NUMBER_INT);
				$id_dom_ext_base=filter_input(INPUT_POST,'id_dom_ext',FILTER_SANITIZE_NUMBER_INT);
						if(mysqli_stmt_bind_param($stmt_select_adversaire,'ii',$id_date_base,$id_equipe_base))
						{
							if(mysqli_execute($stmt_select_adversaire))
							{
							$nbre = mysqli_stmt_affected_rows($stmt_select);
							mysqli_stmt_bind_result($stmt_select,$ligne['id_equipe_adversaire']);
							while(mysqli_stmt_fetch($stmt_select))
							{
								$id_equipe_adversaire = $ligne['id_equipe_adversaire'];
								
							}
											if(mysqli_stmt_bind_param($stmt_update_1,'iii',$id_adversaire_base,$id_num_equipe_base,$id_equipe_adversaire))
											{
												if(mysqli_execute($stmt_update_1))
												{	
													if(mysqli_stmt_bind_param($stmt_select,'ii',$id_adversaire_base,$id_num_equipe_base))
													{
														if(mysqli_execute($stmt_select))
															{
															$nbre = mysqli_stmt_affected_rows($stmt_select);
															mysqli_stmt_bind_result($stmt_select,$ligne['id_adversaire']);
															while(mysqli_stmt_fetch($stmt_select))
															{
																$id_adversaire_update = $ligne['id_adversaire'];
															}
															if(mysqli_stmt_bind_param($stmt_update_2,'iiii',$id_adversaire_update,$id_dom_ext_base,$id_date_base,$id_equipe_base))
															{
																if(mysqli_execute($stmt_update_2))
																{
																$data['resultat'] = $msg['code_ok']['id'];
																}										
																else
																{
																	$data['resultat'] = $msg['code_echec_04']['id'];
																}
															}
															else
															{
															$data['resultat'] = $msg['code_echec_04']['id'];
															}
														}
														else
														{
														//erreur de bind
														$data['resultat'] = $msg['code_echec_06']['id'];
														}
													}
													else
													{
													//erreur de bind
													$data['resultat'] = $msg['code_echec_06']['id'];
													}
												}
												else
												{	/**/
													$data['resultat'] = $msg['code_echec_01']['id'];
												}
											}
											else
											{
											//erreur de bind
											$data['resultat'] = $msg['code_echec_06']['id'];
											}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						}
						else
						{
							//code erreur de prepare
							$data['resultat'] = $msg['code_echec_05']['id'];
						}
						/*}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}*/
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
/*	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}
*/
	mysqli_stmt_close($stmt_update_2);
	mysqli_stmt_close($stmt_select);
	mysqli_stmt_close($stmt_update_1);
	mysqli_stmt_close($stmt_select_adversaire);
	//encodage JSON
	header('Content-Type: application/json');
	echo json_encode($data);	
	mysqli_close($db);	
?>