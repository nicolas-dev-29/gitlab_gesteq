	<!-- Modal lieux descompétritins-->
	<div class="modal fade  "  id="modal_composition_equipe_mail" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_lieux" aria-hidden="true">
		<div class="modal-dialog modal-lg  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h3 class="modal-title">Envoi des convocations aux joueurs</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<h3><span id="modal_composition_equipe_date"> date </span></h3>
					<div id="modal_composition_equipe_liste">
					</div>
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_envoi_mail_button">suivant</button> 
				</div>
			</div>
		</div>
	</div>
	<!-- modal message du coach -->	
	<div class="modal fade  "  id="modal_composition_equipe_mail_message_coach" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_lieux" aria-hidden="true">
		<div class="modal-dialog modal-lg  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h3 class="modal-title">Envoi des convocations aux joueurs</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<h1> message du coach à destinations des joueurs </h1>
					<div >
							<textarea id="contenu_message_coach" class="form-control" ></textarea>
					</div>
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_envoi_mail_button_message_coach">suivant</button> 
				</div>
			</div>
		</div>
	</div>
	<!-- Modal prévisualisation-->
	<div class="modal fade  "  id="modal_composition_equipe_mail_previsualisation" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_lieux" aria-hidden="true">
		<div class="modal-dialog modal-lg  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h3 class="modal-title">Envoi des convocations aux joueurs</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<div id="contenu_mail">
						pas de contenu affiché
					</div>
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_envoi_mail_button_validation">envoyer</button> 
				</div>
			</div>
		</div>
	</div>