<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//mail
	require '../../fonctions/fonction_mail.inc';
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_valeurs = "SELECT `date`,`nom_equipe`,`nom_domext`,`horaire_domext`,`nom_niveau`,`nom_club_A`,`id_equipe`,`nom_date`,`journee_date`,`phase_date` ,`nom_poule`, `num_equipe`,`horaire`,`lieu_rendez_vous`,
	`horaire_rendez_vous`,`adresse_club_A`,`code_postal_club_A`,`ville_club_A`,`tel_club_A`
	FROM table_centrale C 
	LEFT JOIN table_equipes E ON C.equipe_sdstt = E.id_equipe 
	LEFT JOIN table_adversaire A ON C.equipe_adversaire = A.id_adversaire
    LEFT JOIN table_clubs_a TCA ON A.id_adversaire_club_a=TCA.id_club_A
	LEFT JOIN table_domext D ON A.id_domext = D.id_domext 
	LEFT JOIN table_niveaux N ON C.niveau_championnat = N.id_niveau 
	LEFT JOIN table_poule TP ON C.poule =TP.id_poule 
	LEFT JOIN table_date DA ON C.date = DA.id_date 
	LEFT JOIN table_rendez_vous RV ON C.rendez_vous = RV.id_rendez_vous 
	WHERE `date` = ?
	GROUP BY `equipe_sdstt` ";
	$requete_liste_joueurs_equipes = "SELECT DISTINCT `nom_joueur`,`prenom_joueur`,`etat_joueur_final` ,`mail_1_joueur`,`mail_2_joueur`
	FROM `table_centrale` TC
	INNER JOIN table_joueurs TJ
		ON  TC.joueur = TJ.id_joueur
	WHERE (`date`= ? AND `etat_joueur_final`=? AND `equipe_sdstt`=?)
	ORDER BY `nom_joueur` DESC";
	$requete_liste_joueurs_adresses = "SELECT DISTINCT `mail_1_joueur`,`mail_2_joueur`
	FROM `table_centrale` TC
	INNER JOIN table_joueurs TJ
		ON  TC.joueur = TJ.id_joueur
	WHERE (`date`= ? )
	ORDER BY `nom_joueur` DESC";
	$requete_liste_joueurs_repos = "SELECT DISTINCT `nom_joueur`,`prenom_joueur`
	FROM `table_centrale` TC
	INNER JOIN table_joueurs TJ
		ON  TC.joueur = TJ.id_joueur
	WHERE (`date`= ? AND `etat_joueur_initial`=? AND `etat_joueur_final`!=? AND `etat_joueur_final`!=? )
	ORDER BY `nom_joueur` DESC";	
	$requete_liste_joueurs_absent = "SELECT DISTINCT `nom_joueur`,`prenom_joueur`
	FROM `table_centrale` TC
	INNER JOIN table_joueurs TJ
		ON  TC.joueur = TJ.id_joueur
	WHERE (`date`= ? AND `etat_joueur_initial`!=? )
	ORDER BY `nom_joueur` DESC";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//déclaration des variables pour la création du message
	$heure = date('H:i:s',time());
	//$destinataire = 'site.badbat@gmail.com';
	//$expediteur = 'convocations.sdstt@gmail.com';
	//$alias_expediteur ='tennis de table - convocations';
	//
	$data['mail']='';
	$data_mail_logistique = '';
	$data_mail_joueur = '';
	$data_mail_entete = '';
	$data_mail_equipe = '';
	$data_mail_pied =' <br> Merci de me confirmer la bonne réception en répondant à ce message. <br> Bonne chance à tous <br>';
	$data_mail_adresse=array();
	$data_mail_sujet ='Convocation championnat jeune ';
	$data_mail_joueur_repos='Joueurs au repos: ';
	$data_mail_joueur_absent='Joueurs absents: ';
	$data_mail_corps_alt='';
	$entetes = '';
	$liste_destinataire = array();
		// en-têtes expéditeur
		//$entetes .='From:'.$from_adresse_locale_hebergeur."\r\n";
		// en-têtes adresse de retour
		//$entetes .= 'Reply-To:'.$replyto_adresse_locale_hebergeur. "\r\n";
		// personnes en copie
		//$entetes .= 'Cc : '.$adresse_head_coach.''."\r\n";
		//$entetes .= 'Bcc : '.$adresse_sauvegarde.'.'."\r\n";
		// type de contenu HTML
		//$entetes .= "Content-type: text/html; \n";
	//preparation
	$stmt_valeurs = mysqli_prepare($db,$requete_valeurs);
	$stmt_liste_joueurs_equipes = mysqli_prepare($db,$requete_liste_joueurs_equipes);
	$stmt_liste_joueurs_adresses = mysqli_prepare($db,$requete_liste_joueurs_adresses);
	$stmt_liste_joueurs_absent = mysqli_prepare($db,$requete_liste_joueurs_absent);
	$stmt_liste_joueurs_repos = mysqli_prepare($db,$requete_liste_joueurs_repos);
	if($stmt_valeurs)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{	
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	
			{
				//  les données sont valides
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				$message_coach_base=filter_input(INPUT_POST,'message_coach',FILTER_SANITIZE_STRING);
				//acquisition parametre logistique
				if(mysqli_stmt_bind_param($stmt_valeurs,'i',$id_base))
				{
				//execution
				if(mysqli_stmt_execute($stmt_valeurs))	
				{
					mysqli_stmt_store_result($stmt_valeurs);
							mysqli_stmt_bind_result($stmt_valeurs,$ligne['id_date'],$ligne['nom_equipe'],$ligne['nom_domext'],$ligne['horaire_domext'],$ligne['nom_niveau'],$ligne['nom_adversaire'],$ligne['id_equipe'],$ligne['nom_date'],$ligne['journee_date'],$ligne['phase_date'],$ligne['nom_poule'],
							$ligne['num_equipe'],$ligne['horaire'],$ligne['lieu_rendez_vous'],$ligne['horaire_rendez_vous'],$ligne['adresse_club_A'],$ligne['code_postal_club_A'],$ligne['ville_club_A'],$ligne['tel_club_A']);
							$index=0;
							while(mysqli_stmt_fetch($stmt_valeurs))
							{
							$data[$index]['resultat'] = $msg['code_ok']['id'];
							//$date = date_create($ligne['nom_date']);
							if($ligne['nom_domext'] == "domicile")
							{
								$ligne['horaire'] = $horaire_domicile;
							}
							$timestamp = strtotime($ligne['nom_date']);
							$date_formate = $data[$index]['date'] = $semaine[date('w',$timestamp)]." ".date('d',$timestamp)." ".$mois[date('n',$timestamp)]." ".date('Y',$timestamp);
							//lieu suivant domicile ou extérieur
							if($ligne['nom_domext'] == "domicile") //domicile
							{
								$lieu_formate = '';
							}
							else	//match à l'exterieur, on met l'adresse complète
							{
								$lieu_formate = 'salle: '.$ligne['adresse_club_A'].' - '.$ligne['code_postal_club_A'].' '.$ligne['ville_club_A'].' tel: '.$ligne['tel_club_A'];
							}
							$data_mail_entete = '<h3>'.'phase:'.$ligne['phase_date'].'- journée '.$ligne['journee_date'].' -  '.$date_formate.'</h3>';
							if($ligne['nom_adversaire'] == "EXEMPT")
							{
								$data_mail_joueur = '';
								$data_mail_logistique_corps = '';
								$data_mail_logistique_entete='<h4>'.$ligne['nom_equipe'].' - '.$ligne['nom_niveau'].'/'.$ligne['nom_poule'].' - '. $ligne['nom_adversaire'].'</h4><br>';
							}
							else
							{
								$data_mail_logistique_entete='<h4>'.$ligne['nom_equipe'].' - '.$ligne['nom_niveau'].'/'.$ligne['nom_poule'].' contre '.$ligne['nom_adversaire'].' - '.$ligne['num_equipe'].'</h4><br>';
								$data_mail_joueur = '<br> joueurs:';
								$data_mail_logistique_corps = 'match à '.$ligne['nom_domext'].' à '.date('G:i', strtotime($ligne['horaire'])).' - rendez vous '.$ligne['lieu_rendez_vous'].' à '.date('G:i', strtotime($ligne['horaire_rendez_vous'])).'<br>
													'.$lieu_formate.'<br>';
							}
							$data_mail_logistique=$data_mail_logistique_entete.$data_mail_logistique_corps;
							if(mysqli_stmt_bind_param($stmt_liste_joueurs_equipes,'iii',$id_base,$id_etat_joueur_joue,$ligne['id_equipe']))
							{
								//execution
								if(mysqli_stmt_execute($stmt_liste_joueurs_equipes))	
								{
									mysqli_stmt_store_result($stmt_liste_joueurs_equipes);
									mysqli_stmt_bind_result($stmt_liste_joueurs_equipes,$ligne['nom_joueur'],$ligne['prenom_joueur'],$ligne['etat_joueur_final'],$ligne['mail_1_joueur'],$ligne['mail_2_joueur']);
									while(mysqli_stmt_fetch($stmt_liste_joueurs_equipes))
									{
									$data_mail_joueur.=' '.$ligne['nom_joueur'].' '.$ligne['prenom_joueur'].',';	
									}
									$data_mail_joueur.="<br><br>";
									$index++;
								}
								else 	
								{	//échec de l'exécution
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
								//erreur de bind
								$data['resultat'] = $msg['code_echec_06']['id'];
							}
							 $data_mail_equipe .= $data_mail_logistique.$data_mail_joueur;
							} //fin du while
					}
					else 	
					{	//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
				/**********************************************************************/
				//liste des adresses (concerne tous les joueurs absent, présents, repos)
				/**********************************************************************/
				if(mysqli_stmt_bind_param($stmt_liste_joueurs_adresses,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_liste_joueurs_adresses))	
					{
					mysqli_stmt_store_result($stmt_liste_joueurs_adresses);
					mysqli_stmt_bind_result($stmt_liste_joueurs_adresses,$ligne['mail_1_joueur'],$ligne['mail_2_joueur']);
						while(mysqli_stmt_fetch($stmt_liste_joueurs_adresses))
						{
							if($ligne['mail_1_joueur'] !='')
							{
								array_push($data_mail_adresse,$ligne['mail_1_joueur']);
							}
							if($ligne['mail_2_joueur'] !='')
							{
								array_push($data_mail_adresse,$ligne['mail_2_joueur']);
							}
						}
					}
					else 	
					{	//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
				/**********************************************************************/
				//liste des joueurs laissés au repos
				/**********************************************************************/
				if(mysqli_stmt_bind_param($stmt_liste_joueurs_repos,'iiii',$id_base,$id_etat_joueur_present,$id_etat_joueur_joue,$id_etat_joueur_occupe))
				{
					if(mysqli_stmt_execute($stmt_liste_joueurs_repos))	
					{
						mysqli_stmt_store_result($stmt_liste_joueurs_repos);
						mysqli_stmt_bind_result($stmt_liste_joueurs_repos,$ligne['nom_joueur'],$ligne['prenom_joueur']);
						while(mysqli_stmt_fetch($stmt_liste_joueurs_repos))
						{
							$data_mail_joueur_repos.=$ligne['nom_joueur'].' '.$ligne['prenom_joueur'].' - ';
						}
					}
					else 	
					{	//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
				/**********************************************************************/
				//liste des joueurs laissés absents
				/**********************************************************************/
				if(mysqli_stmt_bind_param($stmt_liste_joueurs_absent,'ii',$id_base,$id_etat_joueur_present))
				{
					if(mysqli_stmt_execute($stmt_liste_joueurs_absent))	
					{
						mysqli_stmt_store_result($stmt_liste_joueurs_absent);
						mysqli_stmt_bind_result($stmt_liste_joueurs_absent,$ligne['nom_joueur'],$ligne['prenom_joueur']);
						while(mysqli_stmt_fetch($stmt_liste_joueurs_absent))
						{
							$data_mail_joueur_absent.=$ligne['nom_joueur'].' '.$ligne['prenom_joueur'].' - ';
						}
					}
					else 	
					{	//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}			//liste des joueurs laissés au repos
							//liste des joueurs absents			
			$data_mail_sujet.= $date_formate;
			$data['mail'] =	 	$entetes
								.'<br>------------------------------------------------------'
								.'<br>'
								.$data_mail_sujet
								.'<br>------------------------------------------------------'
								.'<br>'
								.$data_mail_entete
								.$data_mail_equipe
								.$data_mail_joueur_repos
								.'<br>'
								.$data_mail_joueur_absent
								.'<br>'
								.'<br>'.$message_coach_base
								.'<br>'
								.$data_mail_pied;
			}
		}
		else
		{
			//erreur de POST
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	if(isset($_POST['validation']) && $_POST['validation']!="")
	{
		if($_POST['validation'] == 1) // le massage mail est validé par le head coach
		{
		$data['mail'] =	 $data_mail_entete.$data_mail_equipe
								.$data_mail_joueur_repos.'<br>'
								.$data_mail_joueur_absent.'<br>'
								.$message_coach_base.'<br>'
								.$data_mail_pied;
		//$res_mail = mail($data_mail_adresse,$data_mail_sujet,$data['mail'],$entetes);
		//$data_mail_adresse = 'mn.viennot@gmail.com';
		//$res_mail = envoi_mail_simple($expediteur,$alias_expediteur,$data_mail_adresse,$data_mail_sujet,$data['mail'],$data_mail_corps_alt);
		//$data_mail_adresse = array('mn.viennot@gmail.com','nicolas.viennot.sdstt@gmail.com');
		//$adresses_copies=array('nicolas883129@gmail.com','sdstt29@gmail.com');
		$res_mail = envoi_mail_multiple($expediteur,$alias_expediteur,$data_mail_adresse,$adresses_copies,$data_mail_sujet,$data['mail'],$data_mail_corps_alt);
		$data['res_mail']= $res_mail;
		$data['adressses']=$data_mail_adresse;
		}
	}
	else
	{
		//erreur de POST
		$data['resultat'] = $msg['code_echec_01']['id'];
	}
	mysqli_stmt_close($stmt_valeurs);
	mysqli_stmt_close($stmt_liste_joueurs_equipes);
	mysqli_stmt_close($stmt_liste_joueurs_repos);
	mysqli_stmt_close($stmt_liste_joueurs_absent);
	mysqli_stmt_close($stmt_liste_joueurs_adresses);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>