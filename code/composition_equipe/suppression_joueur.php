<?php
	header( 'content-type: text/html; charset=utf-8' );	
	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	$requete_update_joueur = "UPDATE `table_centrale` SET `etat_joueur_final`= ?  WHERE (`joueur` = ? AND `date` = ?)";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_update_joueur = mysqli_prepare($db,$requete_update_joueur);
	if($stmt_update_joueur)
	{
		
		mysqli_stmt_bind_param($stmt_update_joueur,'iii',$id_etat_joueur_final_non_defini,$_POST['id_joueur'],$_POST['id_date']);
		
		//execution
		if(mysqli_stmt_execute($stmt_update_joueur))	
		{
			//$nbre = mysqli_stmt_affected_rows($stmt_update_joueur);
			//mysqli_stmt_bind_result($stmt_update_joueur,$ligne['id_joueur'],$ligne['nom_joueur'],$ligne['prenom_joueur'],$ligne['classement']);
			//$index=0;
			/*while(mysqli_stmt_fetch($stmt_update_joueur))
			{*/
				$data['resultat'] = $msg['code_ok']['id'];

				
			//}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	
	}
			
	mysqli_stmt_close($stmt_update_joueur);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>