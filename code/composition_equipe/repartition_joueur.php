<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_liste = "SELECT DISTINCT `joueur`,`nom_joueur`,`prenom_joueur`,`classement`,`etat_joueur_initial`,`etat_joueur_final`,`equipe_sdstt`
	FROM `table_centrale` TC
	INNER JOIN table_joueurs TJ
		ON  TC.joueur = TJ.id_joueur
	INNER JOIN table_etats_joueurs_init TEJI
		ON  TC.etat_joueur_initial = TEJI.id_etat_joueur
	WHERE (`date`= ? AND `etat_joueur_final`=?)";
	
	$requete_joueurs_occuppes = "SELECT DISTINCT `joueur`,`nom_joueur`,`prenom_joueur`,`classement`,`etat_joueur_initial`,`etat_joueur_final`,`equipe_sdstt`
	FROM `table_centrale` TC
	INNER JOIN table_joueurs TJ
		ON  TC.joueur = TJ.id_joueur
	INNER JOIN table_etats_joueurs_init TEJI
		ON  TC.etat_joueur_initial = TEJI.id_etat_joueur
	WHERE (`date`= ? AND `etat_joueur_final`=?)";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	$stmt_joueurs_occuppes = mysqli_prepare($db,$requete_joueurs_occuppes);
	if($stmt_liste)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{	
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
				{
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
					if(mysqli_stmt_bind_param($stmt_liste,'ii',$id_base,$id_etat_joueur_joue))
					{
						//execution
						if(mysqli_stmt_execute($stmt_liste))	
						{
							$nbre = mysqli_stmt_affected_rows($stmt_liste);
							mysqli_stmt_bind_result($stmt_liste,$ligne['joueur'],$ligne['nom_joueur'],$ligne['prenom_joueur'],$ligne['classement'],$ligne['etat_joueur_initial'],$ligne['etat_joueur_final'],$ligne['equipe_sdstt']);
							$index=0;
							while(mysqli_stmt_fetch($stmt_liste))
							{
								$data[$index]['resultat'] = $msg['code_ok']['id'];
								$data[$index]['id_joueur'] = htmlentities($ligne['joueur'],ENT_QUOTES,'UTF-8');
								$data[$index]['nom_joueur'] = htmlentities($ligne['nom_joueur'],ENT_QUOTES,'UTF-8');
								$data[$index]['prenom_joueur'] = htmlentities($ligne['prenom_joueur'],ENT_QUOTES,'UTF-8');
								$data[$index]['classement'] = htmlentities($ligne['classement'],ENT_QUOTES,'UTF-8');
								$data[$index]['etat_joueur_initial'] = htmlentities($ligne['etat_joueur_initial'],ENT_QUOTES,'UTF-8');
								$data[$index]['etat_joueur_final'] = htmlentities($ligne['etat_joueur_final'],ENT_QUOTES,'UTF-8');
								$data[$index]['equipe_sdstt'] = htmlentities($ligne['equipe_sdstt'],ENT_QUOTES,'UTF-8');
								$index++;
							}
						}
						else 	
						{	//échec de l'exécution
							$data['resultat'] = $msg['code_echec_01']['id'];
						}
					}
					else
					{
						//erreur de bind
						$data['resultat'] = $msg['code_echec_06']['id'];
					}
				}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	die("prepare() failed: ".($mysqli_error($db)));
	}
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>