<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation des requêtes
	$requete_update_1 = "UPDATE `table_centrale` SET 
	`etat_joueur_final`=? 
	WHERE (`date`=? AND`joueur`= ?)";
	$requete_update_2 = "UPDATE `table_centrale` SET 
	`etat_joueur_final`=? 
	WHERE (`date`=? AND`joueur`= ? AND equipe_sdstt = ?)";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//preparation des requetes
	//$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_update_1 = mysqli_prepare($db, $requete_update_1);
	$stmt_update_2 = mysqli_prepare($db, $requete_update_2);
	/*if($stmt_verification)
	{*/
		if(isset($_POST['id_date']) && $_POST['id_date']!="" && isset($_POST['id_joueur']) && $_POST['id_joueur']!="" && isset($_POST['id_equipe']) && $_POST['id_equipe']!="")
		{
			//nettoyage des informations provenant de POST
			if((filter_input(INPUT_POST,'id_date',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_joueur',FILTER_SANITIZE_NUMBER_INT)==FALSE)||(filter_input(INPUT_POST,'id_equipe',FILTER_SANITIZE_NUMBER_INT)==FALSE))
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_date_base=filter_input(INPUT_POST,'id_date',FILTER_SANITIZE_NUMBER_INT);
				$id_joueur_base=filter_input(INPUT_POST,'id_joueur',FILTER_SANITIZE_NUMBER_INT);
				$id_equipe_base=filter_input(INPUT_POST,'id_equipe',FILTER_SANITIZE_NUMBER_INT);
				
				/*
				if(mysqli_stmt_bind_param($stmt_verification,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_verification))
					{
						mysqli_stmt_store_result($stmt_verification);
						$nbre = mysqli_stmt_num_rows($stmt_verification);
						if($nbre == 1)	// l'id est unique et est trouvé
						{*/
							
							if(mysqli_stmt_bind_param($stmt_update_1,'iii',$id_etat_joueur_occupe,$id_date_base,$id_joueur_base))
							{
								if(mysqli_execute($stmt_update_1))
								{	
									if(mysqli_stmt_bind_param($stmt_update_2,'iiii',$id_etat_joueur_joue,$id_date_base,$id_joueur_base,$id_equipe_base))
									{
										if(mysqli_execute($stmt_update_2))
										{
											$data['resultat'] = $msg['code_ok']['id'];	
										}
										else
										{
											$data['resultat'] = $msg['code_echec_01']['id'];
										}
									}
									else
									{
									//erreur de bind
									$data['resultat'] = $msg['code_echec_06']['id'];
									}
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						/*}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}*/
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
/*	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}
*/
	mysqli_stmt_close($stmt_update_1);
	mysqli_stmt_close($stmt_update_2);
	//encodage JSON
	header('Content-Type: application/json');
	echo json_encode($data);	
	mysqli_close($db);	
?>