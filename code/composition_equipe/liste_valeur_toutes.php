<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_valeurs = "SELECT `date`,`nom_equipe`,`nom_domext`,`horaire`,`nom_niveau`,`nom_club_A`,`id_equipe`,`nom_date`,`journee_date`,`phase_date` ,`nom_poule`, `num_equipe`
	FROM table_centrale C 
	LEFT JOIN table_equipes E ON C.equipe_sdstt = E.id_equipe 
	LEFT JOIN table_adversaire A ON C.equipe_adversaire = A.id_adversaire
    LEFT JOIN table_clubs_a TCA ON A.id_adversaire_club_a=TCA.id_club_A
	LEFT JOIN table_domext D ON A.id_domext = D.id_domext 
	LEFT JOIN table_niveaux N ON C.niveau_championnat = N.id_niveau 
	LEFT JOIN table_poule TP ON C.poule =TP.id_poule 
	LEFT JOIN table_date DA ON C.date = DA.id_date 
	WHERE `date` = ?
	GROUP BY `equipe_sdstt` ";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	setlocale(LC_TIME, "fr_FR");
	//preparation
	$stmt_valeurs = mysqli_prepare($db,$requete_valeurs);
	if($stmt_valeurs)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{	
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	
			{
				//  les données sont valides
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				if(mysqli_stmt_bind_param($stmt_valeurs,'i',$id_base))
				{
				//execution
				if(mysqli_stmt_execute($stmt_valeurs))	
				{
					mysqli_stmt_store_result($stmt_valeurs);
					/*$nbre = mysqli_stmt_num_rows($stmt_valeurs);
						if($nbre > 1)
						{*/
							mysqli_stmt_bind_result($stmt_valeurs,$ligne['id_date'],$ligne['nom_equipe'],$ligne['nom_domext'],$ligne['horaire'],$ligne['nom_niveau'],$ligne['nom_adversaire'],$ligne['id_equipe'],$ligne['nom_date'],$ligne['journee_datee'],$ligne['phase_datee'],$ligne['nom_poule'],$ligne['num_equipe']);
							$index=0;
							while(mysqli_stmt_fetch($stmt_valeurs))
							{
							$data[$index]['resultat'] = $msg['code_ok']['id'];
							$data[$index]['id_date'] = 				htmlentities($ligne['id_date'],ENT_QUOTES,'UTF-8');
							$data[$index]['nom_equipe'] = 		htmlentities($ligne['nom_equipe'],ENT_QUOTES,'UTF-8');
							$data[$index]['nom_domext'] = 			htmlentities($ligne['nom_domext'],ENT_QUOTES,'UTF-8');
							//$data[$index]['horaire_domext'] = 		htmlentities($ligne['horaire'],ENT_QUOTES,'UTF-8');
							if($ligne['nom_domext'] == "domicile")
							{
								$ligne['horaire'] = $horaire_domicile;
							}
							
							$data[$index]['horaire_domext'] = 		date('G:i', strtotime($ligne['horaire']));
							$data[$index]['nom_niveau'] = 		htmlentities($ligne['nom_niveau'],ENT_NOQUOTES,'UTF-8');
							$data[$index]['nom_adversaire'] = 		htmlentities($ligne['nom_adversaire'],ENT_QUOTES,'UTF-8');
							$data[$index]['id_equipe'] = 		htmlentities($ligne['id_equipe'],ENT_QUOTES,'UTF-8');
							//$date = date_create($ligne['nom_date']);
							//$date_formate =  date_format($date,'l d F Y');
							//$data[$index]['nom_date'] = $date_formate;
							$timestamp = strtotime($ligne['nom_date']);
							$data[$index]['nom_date'] = $semaine[date('w',$timestamp)]." ".date('d',$timestamp)." ".$mois[date('n',$timestamp)]." ".date('Y',$timestamp);
				
							//$data[$index]['nom_date'] = 		htmlentities($ligne['nom_date'],ENT_QUOTES,'UTF-8');
							$data[$index]['journee_date'] = 		htmlentities($ligne['journee_datee'],ENT_QUOTES,'UTF-8');
							$data[$index]['phase_date'] = 		htmlentities($ligne['phase_datee'],ENT_QUOTES,'UTF-8');
							$data[$index]['nom_poule'] = 		htmlentities($ligne['nom_poule'],ENT_QUOTES,'UTF-8');
							$data[$index]['num_equipe'] = 		htmlentities($ligne['num_equipe'],ENT_QUOTES,'UTF-8');
							$index++;
							}
				/*	}
					else
					{
					// id non trouvé ou non unique
					$data['resultat'] = $msg['code_echec_07']['id'];
					}*/
				}
				else 	
				{	//échec de l'exécution
					$data['resultat'] = $msg['code_echec_01']['id'];
				}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}
		else
		{
			//erreur de POST
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	mysqli_stmt_close($stmt_valeurs);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>