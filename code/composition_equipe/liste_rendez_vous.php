<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	$requete_liste = "SELECT `id_rendez_vous`, `lieu_rendez_vous`,`divers_rendez_vous` FROM `table_rendez_vous` ";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
		//execution
		if(mysqli_stmt_execute($stmt_liste))	
		{
			$nbre = mysqli_stmt_affected_rows($stmt_liste);
			mysqli_stmt_bind_result($stmt_liste,$ligne['id_rendez_vous'],$ligne['nom_rendez_vous'],$ligne['divers_rendez_vous']);
			$index=0;
			while(mysqli_stmt_fetch($stmt_liste))
			{
				$data[$index]['resultat'] = $msg['code_ok']['id'];
				$data[$index]['id'] = htmlentities($ligne['id_rendez_vous'],ENT_QUOTES,'UTF-8');
				$data[$index]['nom_rendez_vous'] = htmlentities($ligne['nom_rendez_vous'],ENT_QUOTES,'UTF-8');
				$data[$index]['divers_rendez_vous'] = htmlentities($ligne['divers_rendez_vous'],ENT_QUOTES,'UTF-8');
				$index++;
			}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
			
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>