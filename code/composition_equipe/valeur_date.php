<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/gesteq_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/gesteq_pattern.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//préparation des requêtes
	$requete_verification="SELECT `id_date` FROM table_date WHERE id_date = ?";
	$requete_liste = "SELECT `nom_date`,`journee_date`,`phase_date` FROM table_date WHERE id_date=?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$nbre=0;
	$data=array();
	
	//preparation des requetes
	$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_liste = mysqli_prepare($db, $requete_liste);
	
	if($stmt_verification)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				
				
				if(mysqli_stmt_bind_param($stmt_verification,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_verification))
					{
						mysqli_stmt_store_result($stmt_verification);
						$nbre = mysqli_stmt_num_rows($stmt_verification);
						if($nbre == 1)	// l'id est unique et est trouvé
						{
							if(mysqli_stmt_bind_param($stmt_liste,'i',$id_base))
							{
								if(mysqli_execute($stmt_liste))
								{		
									mysqli_stmt_bind_result($stmt_liste,$ligne['nom_date'],$ligne['journee_date'],$ligne['phase_date']);
									while(mysqli_stmt_fetch($stmt_liste))
									{
									$data['resultat'] = $msg['code_ok']['id'];
									$data['date_nom'] = htmlentities($ligne['nom_date'],ENT_QUOTES,'UTF-8');
									$data['date_journee'] = htmlentities($ligne['journee_date'],ENT_QUOTES,'UTF-8');
									$data['date_phase'] = htmlentities($ligne['phase_date'],ENT_QUOTES,'UTF-8');
									}
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}


	
	
								

	
	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>