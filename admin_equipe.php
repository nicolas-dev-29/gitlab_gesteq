<!--		grille des presences et absences des joueurs		
				date:01/07/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/gesteq_constante.inc");
	//include ("./code/modal_lieux.php");
	//include ("./code/etat/modal_etat.php");
	include ("./code/toast_perso.php");
?>
<div class="container-fluid ">
	<div class="row">
		<div class="col-lg-12">
			<h1> Désignations des niveaux et des poules </h1>
		</div>
	</div>
	<div class="row align-item-left">
		<div class="col-6 text-right">
		<h4>ajout d'une équipe</h4>
		</div>
		<div class="col-6 text-left">
		<button class="btn btn-primary"  id="ajout_equipe" 
		name="ajout_equipe" title="ajout d\'une équipe" data-toggle="tooltip" data-placement="top"   
		value="ajout_equipe"><span class="fa fa-plus fa-1x "></span>
		</button>
	</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
				<div class="row h2 text-center">
					<div class="col-sm-5">
						nom équipe
					</div>
					<div class="col-sm-1">
						suppression
					</div>
					<div class="col-sm-3">
						niveaux
					</div>
					<div class="col-sm-3">
						poule
					</div>
				</div>
		</div>
	<div class="col-12"id="admin_equipe">
	</div>
	</div>
	<div class="row ">
		<div class=" offset-lg-1 col-lg-10 col-sm-12 ">
			<div class="table-responsive ">
				<table class="table   align-middle text-center table-condensed table-stripped my-3" id="table_grille_adversaires">
					<thead id="table_grille_adversaires_entete">
						<!-- insertion des données par jquery depuis une requéte AJAX -->
					</thead>
					<tbody id="table_grille_adversaires_ligne">
						<!-- insertion des données par jquery depuis une requéte AJAX -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php
	include ("./code/admin_equipe/modal_equipe.php");
	include ("./code/toast_perso.php");
?>
<script src="js/gesteq/admin_equipe.js"></script>
