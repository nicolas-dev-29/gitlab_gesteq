<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- bibliotheques css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/themes/cupertino/jquery-ui.css">
		<link  href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/gesteq_joueurs.css" rel="stylesheet" >
		<link rel="icon" href="favicon.ico" />
		<title>Gestion des équipes </title>
	</head>
	<body>
		<header>
		</header>
		<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
			<div class="container-fluid">		
					<div class="nav-header">
						<a class="navbar-brand" href="index.php" >
							<img src="favicon.ico" width="50" height="50" alt=""> </a>
					</div>
			<h1> Saint-Divy Sport Tennis de Table </h1>
			<h2 class="h1_index_joueurs">Planning des compétitions </h2>
			</div>
		</nav>
		<div id= "banniere_echeances" class="container-fluid">
		</div>
		<div class="container-fluid">
			<h3>L'adresse du site a changé. La nouvelle adresse est:</h3>
			<div>
				<h3>
					<a href="https://www.sdstt.com/gesteq/">https://www.sdstt.com/gesteq/</a>
				</h3>
			</div>
			<div>
				<h3>
				Vous allez être redirigé vers la nouvelle adresse du site.
				</h3>
		</div>
		<footer>
		GesteQ - SDSTT
		15/09/2021 V2.0
		</footer>
	</body>
</html>
 <script type="text/JavaScript">
      setTimeout("location.href = 'https://www.sdstt.com/gesteq/';",3000);
 </script>