<!--		administration de la table joueurs		
				date:26/05/2020
				

-->

	

<?php 

	
	//chargement des constantes 
	include ("./constantes/gesteq_constante.inc");
	
?>

<div class="container-fluid" >

	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des joueurs </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre de joueurs définis:</div>
		<div class="col-lg-1"><span id="nombre_joueurs">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			
			
			<!--<button class="btn btn-primary" id="ajout_joueurs" name="ajout_joueurs"  data-toggle="tooltip" data-placement="top" title="Tooltip on top" value="ajout_joueurs"> 
				<span id="ajout_joueurs_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter un lieu 
			</button> -->
			
			<button class="btn btn-primary"  id="ajout_joueurs" name="ajout_joueurs" data-toggle="tooltip" data-placement="top"
                title="ajout d'un lieu" 	value="ajout_joueurs">	
				<span id="ajout_joueurs_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter un joueur
			</button>
			<button class="btn btn-danger"  id="suppression_joueurs_tous" name="suppression_joueurs_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de tous les joueurs"  	value="suppression_joueurs_tous">	
				<span id="suppression_joueurs_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer tous les joueurs
			</button>
					
		</div>
	</div>

	<!-- affichage du tableau de la liste des joueurs -->

	<div  class="row align-items-center" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des joueurs présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  align-middle text-center table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	nom 			</th>
							<th scope="col">	prénom 			</th>
							<th scope="col">	classement		</th>
							<th scope="col">	sélection		</th>
							<th scope="col">	modification	</th>
							<th scope="col">	suppression		</th>
						</tr>
					</thead>
					<tbody id="table_joueurs">
						
						<!-- insertion des données par jquery depuis une requête AJAX -->
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	

	<!-- Modal suppression de tous les joueurs -->
		
	<div class="modal fade" id="modal_suppression_joueurs_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_joueurs_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les joueurs</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les joueurs de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_joueurs_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>
	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/admin_joueurs/modal_joueurs.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	
	
<script src="./js/gesteq/common_admin_joueur.js"></script> 
<script src="js/gesteq/admin_joueur.js"></script> 