<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- bibliotheques css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/themes/cupertino/jquery-ui.css">
		<link  href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/gesteq_joueurs.css" rel="stylesheet" >
		<!-- chargement des bibliotheques js -->
		<script src="js/jquery-3.5.1.min.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script src="js/popper.min.js"></script>		<!--inclusion de la bibliotheques poppover -->
		<script src="js/bootstrap.min.js"></script>
		<!-- chargement des constantes du site -->
		<?php
		include ("./constantes/gesteq_constante.inc");
		include ("./constantes/dictionnaire.inc");
		?>
		<link rel="icon" href="favicon.ico" />
		<title>Gestion des équipes </title>
		
		
	</head>
	<body>
		<header>
		
		</header>
		<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
			<div class="container-fluid">		
					<div class="nav-header">
						<a class="navbar-brand" href="index.php" >
							<img src="favicon.ico" width="50" height="50" alt=""> </a>
					</div>
			<h1> Saint-Divy Sport Tennis de Table </h1>
			<h2 class="h1_index_joueurs">Planning des compétitions </h2>
			</div>
		</nav>
		<div id= "banniere_echeances" class="container-fluid">
		</div>
		<div class="container-fluid">
		<h3> veuillez choisir votre nom et votre prénom dans la liste déroulante </h3>
			<div class="row ">
				<div class="offset-lg-3 col-lg-6">
					<form>
					<label for="admin_user_nom">sélectionnez votre nom et votre prénom: </label>
					<select class=" custom-select" id="admin_user_nom">
					<option value="-1">   </option>
					</select>
					</form>
				</div>

			</div>
			<div class="row formulaire_joueur">
				<div class="col-lg-12 text-center my-3">
				<span id="user_choisi"></span>
				</div>
				
			</div>
			<div class="col-12 formulaire_joueur"id="admin_user">
			</div>
			</div>
			<div class="col-12 text-center formulaire_joueur"id="user_validation">
				<button class="btn btn-danger "  id="user_annulation_button" name="user_annulation_button" data-toggle="tooltip" data-placement="top"
                title="annuler les modifications"  	value="user_annulation_button">	
				<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				annuler
				</button>
				<button class="btn btn-success "  id="user_validation_button" name="user_validation_button" data-toggle="tooltip" data-placement="top"
                title="envoi les informations"  	value="user_validation_button">	
				<span id="attente_envoi_mail" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				envoyer les informations
				</button>
				
				
			</div>

	</div>
			
		</div>
		<footer>
		GesteQ - SDSTT
		17/08/2020 V1.0
		</footer>
	</body>
	<?php
	include ("./code/index_joueurs/modal_user_mail.php");
	include ("./code/toast_perso.php");
	include ("./code/index_joueurs/toast_perso_envoi_mail.php");
?>
	<script src="js/gesteq/index_joueurs.js"></script>
</html>