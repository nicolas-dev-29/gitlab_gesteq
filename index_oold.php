<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- bibliotheques css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/themes/cupertino/jquery-ui.css">
		<link  href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/gesteq.css" rel="stylesheet" >
		<!-- chargement des bibliotheques js -->
		<script src="js/jquery-3.5.1.min.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script src="js/popper.min.js"></script>		<!--inclusion de la bibliotheques poppover -->
		<script src="js/bootstrap.min.js"></script>
		<!-- chargement des constantes du site -->
		<?php
		include ("./constantes/gesteq_constante.inc");
		include ("./constantes/dictionnaire.inc");
		?>
		<title>Gestion des équipes </title>
	</head>
	<body>
		<header>
		GesteQ - gestion des équipes 
		</header>
		<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
			<div class="container-fluid">		
					<div class="nav-header">
						<a class="navbar-brand" href="index.php" > GesteQ </a>
					</div>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_restreinte">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end" id="navbar_restreinte">
						<ul class="nav navbar-nav ">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> calendrier </a>
								<div class="dropdown-menu">
								<!--	<a class="dropdown-item" href="index.php?page=creation_fiche.php"> création d'une fiche</a>
									<a class="dropdown-item" href="index.php?page=gestion_fiche.php">gestion d'une fiche </a> -->
								</div>
							</li>
							<li class="nav-item">			<a class="nav-link" href="index.php?page=grille_presence.php">		grille des présences</a></li>
							<li class="nav-item">			<a class="nav-link" href="index.php?page=composition_equipe.php">		composition</a></li>
							<li class="nav-item">			<a class="nav-link" href="index.php?page=communications.php">		communications</a></li>
							<li class="nav-item">			<a class="nav-link" href="index.php?page=creation_test.php">		creation_test</a></li>							
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown"> administration base  </a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="index.php?page=admin_clubs.php">					clubs</a>
									<a class="dropdown-item" href="index.php?page=admin_equipe.php">			équipe SDSTT</a>
									<a class="dropdown-item" href="index.php?page=admin_adversaires.php">			adversaires</a> 
									<a class="dropdown-item" href="index.php?page=admin_joueurs.php">				joueurs</a>
									<a class="dropdown-item" href="index.php?page=admin_etats.php">					états</a> 
									<a class="dropdown-item" href="index.php?page=admin_calendrier.php">			calendrier</a>
									<a class="dropdown-item" href="index.php?page=admin_championnats.php">			championnat</a> 
									<a class="dropdown-item" href="index.php?page=admin_niveaux.php">				niveaux</a>
									<a class="dropdown-item" href="index.php?page=admin_domext.php">				domicile/ext</a> 
								</div>
							</li>
						</ul>
					</div>
			</div>
		</nav>
		<div id= "banniere_echeances" class="container-fluid">
		</div>
		<div class="container-fluid">
			<?php
			//test de la variable page et affichage du contenu de la page
			if (isset($_GET['page']) && $_GET['page']!="" ) 
			{
				$page = $_GET['page'];
				if(in_array($page,$pages_autorisees))
				{
					include ($page);
				}
				else
				{?>
					<h1> Page introuvable </h1>
				<?php
				}
			}
			else
			{?>
				<h1>accueil </h1>
			<?php
			}
			?>				
		</div>
		<footer>
		GesteQ - SDSTT
		26/05/2020 V0.1
		</footer>
	</body>
</html>