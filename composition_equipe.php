<!--		composition des équipes par journées		
				date:06/05/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/gesteq_constante.inc");
	//include ("./code/modal_lieux.php");
	//include ("./code/etat/modal_etat.php");
	
?>

<!----------------------------------------------------------------------------->
<!-- 		partie coummune à toutes les journées 							--->
<!----------------------------------------------------------------------------->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1> Composition des équipes </h1>
		</div>
	</div>
	<div class="row align-item_center">
		<div class="col-sm-4">
			<h3> choix de la journée </h3>
		</div>
		<div class="col-sm-8">
			<div class="form-row">
				<div class="col-lg-6">
					<select id="composition_equipe_date_liste_select" class="custom-select ">
						<option selected value="-1"> entrer le numéro de la journée (ex: J1) </option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row align-item_center">
		<div class="col-lg-12">
			<h1>phase <span id="composition_equipe_date_phase">X</span> - journée n°<span id="composition_equipe_date_journee">X</span> - <span id="composition_equipe_date_nom">X</span> </h1>
		</div>
	</div>
	<!-- bandeau de boutons pour validation/RAZ/envoi -->
	<div class="row">
		<div class="col-lg-6 text-center">

		</div>
		<div class="col-lg-6 text-center my-3">
			<button class="btn btn-success "  id="composition_equipe_envoi_mail_joueurs" name="composition_equipe_envoi_mail_joueurs" data-toggle="tooltip" data-placement="top"
                title="envoi le mail aux joueurs sélectionnés"  	value="composition_equipe_envoi_mail_joueurs">	
				<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				envoyer mail aux joueurs
			</button>
			<!--<button class="btn btn-secondary disabled"  id="composition_equipe_envoi_convocation_site" name="composition_equipe_envoi_convocation_site" data-toggle="tooltip" data-placement="top"
                title="envoi les convocations sur le site sdstt.com"  	value="composition_equipe_envoi_convocation_site">	
				<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				<!--envoyer convocation sur le site
			</button>-->
		</div>
	</div>
	<!-- corps de la page: joueurs présents / équipes -->
	
	<div class="row align-items-stretch">
	<!-- liste des absents -->
		<div class="col-lg-6 encadre">
			<h5> liste des absents: <span id="nbre_absents"></span> </h5>
			<span id="liste_absents">vide</span>
		</div>
		<div class="col-lg-6 encadre">
			<h5> liste des non définis: <span id="nbre_non_definis"></span> </h5>
			<span id="liste_non_definis">vide</span>
		</div>
	</div>
	<!-- liste des"oui si nécessaire -->
		<div class="row align-items-stretch">
	<!-- liste des absents -->
		<div class="col-lg-12 my-3 encadre">
			<h5> liste des "oui si nécessaire: <span id="nbre_oui_necessaire"><span></h5>
			<span id="liste_oui_necessaire"></span>
		</div>

		</div>
	
	<div id="encadre_nbre_presents" class="row align-items-stretch element_cache_debut">
		<div class="col-lg-12 my-3 encadre ">
		<h3> nombre de joueurs présents: <span id="nbre_presents"><span></h3>
		</div>
	</div>
	<div class="row align-items-stretch">
		<div class="col-sm-4 encadre my-3">
			<h3> liste des joueurs disponibles: <span id="nbre_disponibles"></span> </h3>
			<div id="composition_equipe_liste_joueurs_disponible" >

			</div>
		</div>
			<!-- remplit par le jquery -->
		<div class="col-sm-8 encadre my-3 ">
			<h3> liste des équipes </h3>
			<div id="composition_equipe_liste_equipe">
				<div id="grille_equipes" class="card-columns">
				</div>
			</div>
		</div>
	</div>

</div>
<?php
	include ("./code/composition_equipe/modal_composition_equipe_mail.php");
	include ("./code/toast_perso.php");
?>
<script src="js/gesteq/composition_equipe.js"></script>