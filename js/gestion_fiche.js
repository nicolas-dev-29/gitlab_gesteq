$(function(){



var tab_msg={};
$('[data-toggle="tooltip"]').tooltip();


$.getScript("./js/badbat/gestion_fiche_creation_modification_nav_1.js");

function mise_a_jour_liste()
{	
		$.getJSON('constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				
		//mise à jour du tableau des batteries avec les paramètres:
		// tension, equipement, lieux, date de MES, date de retrait
	var lieu_recherche = $('#recherche_fiche_lieux').val();
	$.ajax({
		url      	: "code/gestion_fiche_liste_2.php",
		type   		: "POST",	
		data     	: {lieu: lieu_recherche},		
		cache    	: false,
		async		: true,
		dataType 	: "json",
		error    	: function(request, error) { // Info Debuggage si erreur         
					   alert("Erreur : responseText: "+request.responseText);
					 },
		success  	:function(reponse) 
					{  
						$('#consultation_fiche_nombre').html("<b>"+reponse.length+"</b>");
						$('#gestion_fiche_tableau tr').each(function(){
							$(this).remove();
						});
						var button_consulter='<button class="btn btn-primary gestion_fiche_consulter" name="consultation_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="consultation de la fiche de la batterie"	value="consulatation_fiche_batterie">	<span class="fa fa-eye fa-1x "></span></button>';
						var button_modifier='<button class="btn btn-warning gestion_fiche_modifier" name="modifier_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="modification de la fiche de la batterie"	value="modification_fiche_batterie">	<span class="fa fa-edit fa-1x "></span></button>';
						var button_supprimer='<button class="btn btn-danger gestion_fiche_supprimer" name="suppression_fiche_batterie" title="suppression de la fiche batterie" data-toggle="tooltip" data-placement="top"   value="suppression_fiche_batterie"><span class="fa fa-trash fa-1x "></span></button>';
						var ligne_table='';
						$.each(reponse,function(i,item){
							if(item.resultat ==  tab_msg['code_ok']['id'])
							{
								ligne_table +='<tr class="consultation_fiche" id='+item.id+'><td>'+ item.references + '</td><td>' + item.tension+ 'V</td><td>'+ item.equipements+ '</td><td>'+ item.lieux+ '</td><td>'+ item.date_mes+ '</td><td>'+button_consulter+'</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
							}
							else
							{
							var res = item.resultat;
							var message = tab_msg[res]['texte'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						});
						$('#gestion_fiche_tableau').append(ligne_table);
					}						
			});	
			});
}
$(document).ready(function(){
			//$('#validation_fiche_spinner').hide();
			//on cache les toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			
			$('.gestion_fiche_consultation').hide();
			$('.gestion_fiche_modification').hide();
			$('#gestion_fiche_retour_liste').hide();
			$('#recherche_fiche_lieux').show();		
		
		
			
			$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				});
			mise_a_jour_liste();
});
/*************************************************/
//recherche de la fiche
/************************************************/
$('#recherche_fiche_lieux').autocomplete({
	minLength:		0,
	select:			function(event,ui){
					$('#recherche_fiche_lieux').val(ui.item.value);
					mise_a_jour_liste();
					}, 	//action de selection
	response: 		function( event, ui ) {
					//console.log($(this).val());
					var lieu_recherche =$(this).val();
					//console.log("response:"+lieu_recherche);
					$.ajax({
						url      	: "code/gestion_fiche_liste_2.php",
						type   		: "POST",	
						data     	: {lieu: lieu_recherche},		
						cache    	: false,
						async		: true,
						dataType 	: "json",
						error    	: function(request, error) { // Info Debuggage si erreur         
									   alert("Erreur : responseText: "+request.responseText);
									 },
						success  	:function(reponse) 
									{  	//console.log(reponse);
										$('#consultation_fiche_nombre').html("<b>"+reponse.length+"</b>");
										$('#gestion_fiche_tableau tr').each(function(){
											$(this).remove();
										});
										var button_consulter='<button class="btn btn-primary gestion_fiche_consulter" name="consultation_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="consultation de la fiche de la batterie"	value="consulatation_fiche_batterie">	<span class="fa fa-eye fa-1x "></span></button>';
										var button_modifier='<button class="btn btn-warning gestion_fiche_modifier" name="modifier_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="modification de la fiche de la batterie"	value="modification_fiche_batterie">	<span class="fa fa-edit fa-1x "></span></button>';
										var button_supprimer='<button class="btn btn-danger gestion_fiche_supprimer" name="suppression_fiche_batterie" title="suppression de la fiche batterie" data-toggle="tooltip" data-placement="top"   value="suppression_fiche_batterie"><span class="fa fa-trash fa-1x "></span></button>';
										var ligne_table='';
										$.each(reponse,function(i,item){
											if(item.resultat ==  tab_msg['code_ok']['id'])
											{
												ligne_table +='<tr class="consultation_fiche" id='+item.id+'><td>'+ item.references + '</td><td>' + item.tension+ 'V</td><td>'+ item.equipements+ '</td><td>'+ item.lieux+ '</td><td>'+ item.date_mes+ '</td><td>'+button_consulter+'</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
											}
											else
											{
											var res = item.resultat;
											var message = tab_msg[res]['texte'];
											$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
											$('#toast_enregistrement_echec').toast('show');
											}
										});
										$('#gestion_fiche_tableau').append(ligne_table);
									}						
							});	
					},
	
	source:			function(requete,reponse){
						//récupération de la liste des lieux
					$.ajax({
								url			:"code/recherche_lieux_liste.php", 
								type		:"POST",
								cache		:false,
								data		:{lieu: requete.term},
								dataType	:"json",
								error		: function(request,error){
											alert("Erreur : responseText: "+request.responseText);
											},
								success		: function(donne){
													reponse(donne);
													//console.log(donne);
											}
							});
					}
});
/*********************************************************************************************************************************************************************/
// 
//							bandeau principal
//
//
//boutons généraux - validation, raz et annulation , création d'une nouvelle fiche
/*********************************************************************************************************************************************************************/

/************************************************/
// bouton: validation
/************************************************/
// bouton: effacer la recherche
$('#raz_recherche_fiche').on('click',function(e){
	$('#recherche_fiche_lieux').val("");
	//$('#gestion_fiche_tableau tr').each(function(){	$(this).remove();});
	mise_a_jour_liste();
	$(this).blur();
});
/************************************************/
// bouton: annulation recherche
$('#annulation_recherche_fiche').on('click',function(e){
	$('.gestion_fiche_liste').hide();
	$('.gestion_fiche_consultation').hide();
	$('.gestion_fiche_modification').hide();
	$('#gestion_fiche_retour_liste').hide();
	$(this).blur();
	$(location).attr('href','index.php');
});
	//bouton retour à la liste
	$('#gestion_fiche_retour_liste').on('click',function(){
		$('.gestion_fiche_consultation').hide();
		$('.gestion_fiche_modification').hide();
		$('#gestion_fiche_retour_liste').hide();
		
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_validation_fiche').attr('disabeld',false);
		
		$('.gestion_fiche_liste').show();
		$('#recherche_fiche_lieux').show();
		$('#gestion_fiche_titre').text("Liste des fiches");
		$(this).blur();
		mise_a_jour_liste();
	});	
	
	//bouton création d'une nouvelle fiche
	$('#gestion_fiche_creation_fiche').on('click',function(){
		$('.gestion_fiche_consultation').hide();
		$('.gestion_fiche_modification').hide();
		$('#gestion_fiche_retour_liste').hide();
		$('.gestion_fiche_liste').hide();
		$('#gestion_fiche_titre').text("Liste des fiches");
		$(location).attr('href','./index.php?page=creation_fiche.php');
	});	
	
/*****************************************************************/
// boutons du tableau - consultation, modification et suppression
/*****************************************************************/
	//consultation
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_consulter',function(e){
		$(this).blur();		
		$('.gestion_fiche_liste').hide();
		$('.gestion_fiche_modification').hide();
		$('#recherche_fiche_lieux').hide();
		$('.gestion_fiche_consultation').show();
		$('#gestion_fiche_retour_liste').show();
		$('#gestion_fiche_reference').show();
		$('#nav-1').addClass('active');
		$('#gestion_fiche_titre').text("Consultation d'une fiche");

		 //mise à jour des valeurs
		 var id_recup  = 	$(e.currentTarget).parent().parent().attr("id");
		 //console.log("valeur id envoyé:"+id_recup);
		 $.ajax({
			url      	: "code/gestion_fiche_consultation_valeurs.php",
			type   		: "POST",
			data     	: {id: id_recup},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							//console.log(retour_json);

							$('#gestion_fiche_lieux_nom').text(retour_json['nom_lieux']);
							$('#gestion_fiche_lieux_divers').text(retour_json['divers_lieux']);
							$('#gestion_fiche_reference').text(retour_json['tension']+'-'+retour_json['equipements']+'-'+retour_json['nom_lieux']);
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							
							mise_a_jour_liste();
							}
						}													
				});	

		
});
	//modification
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_modifier',function(e){
		
		$('.gestion_fiche_liste').hide();
		$('#recherche_fiche_lieux').hide();	

		
		$('#gestion_fiche_reference').show();
		$('.gestion_fiche_modification').show();
		$('#gestion_fiche_retour_liste').show();
		$('#gestion_fiche_titre').text("Modification d'une fiche");
		$('nav-1').addClass('active');
		
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_validation_fiche').attr('disabeld',false);		
		$(this).blur();
		//mise en place des valeurs
		var id_recup = 	$(e.currentTarget).parent().parent().attr("id");
		tab_provisoire['id_batteries']= id_recup;
		
		//console.log("valeur id envoyé:"+id_recup);
		$('#creation_fiche_lieux_liste_select').empty();
		
		$('#validation_fiche_spinner').hide();
			$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				
		//création du tab_provisoire et du tab_provisoire_initiale (pour le retour arrière)
		 $.ajax({
			url      	: "code/gestion_fiche_consultation_valeurs.php",
			type   		: "POST",
			data     	: {id: id_recup},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							tab_provisoire['id_lieux'] = retour_json['id_lieux'];
							tab_provisoire['id_equipement'] = retour_json['id_equipements'];
							tab_provisoire['id_tension'] = retour_json['id_tension'];
							tab_provisoire['id_batteries']=id_recup;
						
							tab_provisoire['lieux']=retour_json['nom_lieux'];
							tab_provisoire['equipement']=retour_json['equipements'];
							tab_provisoire['tension']=retour_json['tension'];
							
							tab_provisoire_initiale['id_lieux'] = retour_json['id_lieux'];
							tab_provisoire_initiale['id_equipement'] = retour_json['id_equipements'];
							tab_provisoire_initiale['id_tension'] = retour_json['id_tension'];
							tab_provisoire_initiale['id_batteries']=id_recup;
						
							tab_provisoire_initiale['lieux']=retour_json['nom_lieux'];
							tab_provisoire_initiale['equipement']=retour_json['equipements'];
							tab_provisoire_initiale['tension']=retour_json['tension'];

							//console.log("tab: "+tab_provisoire);
							$('#gestion_fiche_reference').text(retour_json['tension']+'-'+retour_json['equipements']+'-'+retour_json['nom_lieux']);
							
							//insertion de la liste des lieux après le success précédent pour être sur d'avoir les bnonnes valeurs dans le tab_provisoire
							$.ajax({
									url			:"code/admin_lieux_liste.php", //on utilise la page admin_lieux
									type		:"POST",
									cache		:false,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse){
													var ligne_select='';
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	//console.log("item.id:"+item.id+" tab_provisoire:"+tab_provisoire['id_lieux']);
															if((item.id)==(tab_provisoire['id_lieux']))
															{
																ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
																
															}
															else
															{
																ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													ligne_select+='<option value="-1"> entrer le lieu </option>'
													$('#creation_fiche_lieux_liste_select').append(ligne_select);
												}
									});

							
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						}													
				});			
			});
		}); //fin de modifier
		
		

	//
	/*************************************************/
// boutons généraux - validation, raz et annulation
/************************************************/


/************************************************/
// bouton: validation
/************************************************/
$('#gestion_fiche_validation_fiche').on('click',function(){
	var json_tab = JSON.stringify(tab_provisoire);
	//insertion des éléments dans la base à partir des index
	$.ajax({
				url			:"code/gestion_fiche_validation_fiche.php", 
				type		:"POST",
				data		:{tab: json_tab},
				cache		:false,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(retour_json){
							$('#validation_fiche_spinner').hide();
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						}	
	});
	$(this).blur();
	$(this).addClass('disabled');
	$(this).attr('disabled',true);
	$('#gestion_fiche_raz_fiche').attr('disabled',true);
	$('#gestion_fiche_raz_fiche').addClass('disabled');

});

	//************************************************************/
	//RAZ - retour des valeurs initiales
	/******************************************************************/
	$('#gestion_fiche_raz_fiche').on('click',function(){
		$.each(tab_provisoire,function(index,d){
			tab_provisoire[index]="";
		});	
		
		//retour aux valeurs initiales de toutes les valeurs
		tab_provisoire['id_lieux'] = tab_provisoire_initiale['id_lieux'];
		tab_provisoire['id_equipement'] = tab_provisoire_initiale['id_equipements'];
		tab_provisoire['id_tension'] = tab_provisoire_initiale['id_tension'];
		tab_provisoire['id_batteries']=tab_provisoire_initiale['id_batteries'];
	
		tab_provisoire['lieux']=tab_provisoire_initiale['lieux'];
		tab_provisoire['equipement']=tab_provisoire_initiale['equipements'];
		tab_provisoire['tension']=tab_provisoire_initiale['tension'];
		
		$('#creation_fiche_lieux_liste_select').val(tab_provisoire_initiale['id_lieux']);
		mise_a_jour_reference();
		
	$(this).blur();
	});


	//************************************************************/
	//suppression
	/******************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_supprimer',function(e){
	//récupération des informations
		var id_recup = 			$(e.currentTarget).parent().parent().attr("id");
		var reference_recup = 	$(e.currentTarget).parent().parent().find("td").eq(0).html();
		var tension_recup = 	$(e.currentTarget).parent().parent().find("td").eq(01).html();
		var equipement_recup = 	$(e.currentTarget).parent().parent().find("td").eq(02).html();
		var lieu_recup = 		$(e.currentTarget).parent().parent().find("td").eq(03).html();
		var date_recup = 		$(e.currentTarget).parent().parent().find("td").eq(04).html();
		$('#gestion_fiche_titre').text("Suppression d'une fiche");
		//initialisation du modal
		$('#modal_suppression_gestion_fiche_id').attr("value",id_recup);
		$('#modal_suppression_gestion_fiche_reference').text(reference_recup);
		$('#modal_suppression_gestion_fiche_tension_reseau').text(tension_recup);
		$('#modal_suppression_gestion_fiche_equipement').text(equipement_recup);
		$('#modal_suppression_gestion_fiche_lieu').text(lieu_recup);
		$('#modal_suppression_gestion_fiche_date_mes').text(date_recup);
	//lancement de la modal	
		$('#modal_suppression_gestion_fiche').modal('show');
		$(this).blur();
	});
			$('#modal_suppression_gestion_fiche').on('hide.bs.modal',function(){
				$('#gestion_fiche_titre').text("Liste des fiches");
			});
	// suppression de la fiche
		$('#modal_suppression_gestion_fiche_validation_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var id_supprime = $('#modal_suppression_gestion_fiche_id').attr("value");
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/gestion_fiche_suppression.php",
			type   		: "POST",
			data     	: {id: id_supprime},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
	
	
/************************************************/
// bouton: interdiction pendant le chargement
/************************************************/
	$('#toast_enregistrement_ok').on('hidden.bs.toast',function(){
		$('#gestion_fiche_validation_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_raz_fiche').removeClass('disabled');


	});
	$('#toast_enregistrement_echec').on('hidden.bs.toast',function(){
		$('#gestio_fiche_validation_fiche').attr('disabled',false);
		$('#gestio_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_raz_fiche').removeClass('disabled');

	});
	
	
});