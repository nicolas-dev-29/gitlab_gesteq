var tab_provisoire={};
var tab_provisoire_initiale={};
var tab_msg={};
var tab_etat_initial_joueur=[];
function acqusition_echange()
{
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				});
}
/************************************************************************************************************/
//	chargement de la page
// 			chargement des donnees
/************************************************************************************************************/
		$(function(){
			//masque des toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			$('#table_grille_presence_entete').empty();
			$('#table_grille_presence_ligne').empty();
			$('#modal_ajout_equipe').modal('hide');
			//masque des spinners
			$.ajax({
				url		:"./constantes/code_message.json",
				Type	:"GET",
				cache	:false,
				dataType:"json",
				error		: function(request,error){
											alert("Erreur : responseText: "+request.responseText);
											},
				success		:function(data)
							{
							var tab_msg={};
							$.each(data,function(index,d){			
								var tab_msg_tampon={};
								tab_msg_tampon['id']= d.id;
								tab_msg_tampon['nom']=d.nom;
								tab_msg_tampon['texte']=d.texte;
								tab_msg[index]=tab_msg_tampon;
								});
							////console.log(tab_msg);
							var ligne='';
							var ligne_select='';
							var ligne_form_niveaux='';
							var ligne_form_poules='';
							var index_tab=01;
							var button_supprimer='<button class="btn btn-danger  suppression_equipe" name="suppression_equipe" title="suppression d\'une équipe" data-toggle="tooltip" data-placement="top"   value="suppression_equipe"><span class="fa fa-trash fa-1x "></span></button>';
							//r�cupration de la liste des �tats
								$.ajax({
											url			:"./code/niveaux/admin_niveaux_liste.php", 
											type		:"POST",
											cache		:false,
											dataType	:"json",
											error		: function(request,error){
														alert("Erreur : responseText: "+request.responseText);
														},
											success		: function(reponse)
														{
															ligne_form_niveaux+='<select  class="custom-select select_niveau">\
																			<option value="-1">   </option>'
															$.each(reponse,function(i,item){
															////console.log(item.resultat);
															if(item.resultat ==  tab_msg['code_ok']['id'])
															{
																ligne_form_niveaux +='<option  value='+item.id_niveau+'> '+item.nom_niveau+' </option>';
															}
															else
															{
																alert("erreur");
																var res = item.resultat;
																var message = tab_msg[res]['texte'];
																$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																$('#toast_enregistrement_echec').toast('show');
															}
															});
															ligne_form_niveaux+='</select>';
															$.ajax({
																	url			:"./code/poules/admin_poules_liste.php", 
																	type		:"POST",
																	cache		:false,
																	dataType	:"json",
																	error		: function(request,error){
																				alert("Erreur : responseText: "+request.responseText);
																				},
																	success		: function(reponse)
																				{
																					ligne_form_poules+='<select  class="custom-select select_poule">\
																									<option value="-1">   </option>'
																					$.each(reponse,function(i,item){
																					////console.log(item.resultat);
																					if(item.resultat ==  tab_msg['code_ok']['id'])
																					{
																						ligne_form_poules +='<option  value='+item.id_poule+'> '+item.nom_poule+' </option>';
																					}
																					else
																					{
																						alert("erreur");
																						var res = item.resultat;
																						var message = tab_msg[res]['texte'];
																						$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																						$('#toast_enregistrement_echec').toast('show');
																					}
																					});
																					ligne_form_poules+='</select>';
															$.ajax({
																	url			:"./code/composition_equipe/liste_equipe.php", 
																	type		:"POST",
																	cache		:false,
																	dataType	:"json",
																	error		: function(request,error){
																				alert("Erreur : responseText: "+request.responseText);
																				},
																	success		: function(reponse)
																				{
																					console.log(reponse);
																					//var ligne_entete='';
																					//ligne_entete+='<th col=\"scope\">joueur  </th>'
																					$.each(reponse,function(i,item){
																					////console.log(item.resultat);
																					if(item.resultat ==  tab_msg['code_ok']['id'])
																					{
																						//ligne_entete +='<th col=\"scope\" id='+item.id+'> J'+item.journee+' / '+item.date+'</th>';
																						//ligne_select +='<td id='+index_tab+'>'+ligne_form+'</td>';
																						ligne+='<div class="row my-3  align-items-center ligne_niveau_poule  text-center" id='+item.id+'>\
																									<div class=" h5 col-sm-5">'+item.nom_equipe+'</div>\
																									<div class="col-sm-1">'+button_supprimer+'</div>\
																									<div class="col-sm-3">'+ligne_form_niveaux+'</div>\
																									<div class="col-sm-3">'+ligne_form_poules+'</div>\
																								</div>';
																								index_tab++;
																					}
																					else
																					{
																						alert("erreur");
																						var res = item.resultat;
																						var message = tab_msg[res]['texte'];
																						$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																						$('#toast_enregistrement_echec').toast('show');
																					}
																					});
																					$('#admin_equipe').append(ligne);
																					$.ajax({
																						url			:"./code/admin_equipe/liste_niveau_poule.php", 
																						type		:"POST",
																						cache		:false,
																						dataType	:"json",
																						error		: function(request,error){
																									alert("Erreur : responseText: "+request.responseText);
																									},
																						success		: function(reponse)
																									{
																									//console.log(reponse);
																									$(".ligne_niveau_poule").each(function(i,index){
																										////console.log("L:"+$(this).parent().prop("id")+"/C: "+$(this).parent().parent().prop("id"));
																										//var ligne = $(this).parent().parent().prop("id");
																										//var colonne = $(this).parent().prop("id");
																											$(this).find('.select_niveau').find('option[value="'+reponse[i]['niveau_championnat']+'"]').prop('selected', true);
																											$(this).find('.select_poule').find('option[value="'+reponse[i]['poule']+'"]').prop('selected', true);
																										});
																									}
																					});
																				}
																	});
														}
														});
														}
									});			
							}
				});
				acqusition_echange();
//affichage des valeurs initiales
		});
//r�cup�ration de la valeur de select sur un changmement de niveau_championnat
$('#admin_equipe').on("change",".select_niveau",function(e){
		acqusition_echange();
		var id_etat_selectionne = $(e.currentTarget).val();
		//var id_colonne = $(e.currentTarget).parent().attr("id");
		var id_equipe = $(e.currentTarget).closest('.ligne_niveau_poule').attr("id");
		////console.log("id colonne (date):"+id_colonne);
		//console.log("id ligne:"+id_equipe);
		//console.log("id etat retenu :"+id_etat_selectionne);
								//mise � jour de l'�tat du joueur dans la base imm�diatement
								$.ajax({
										url			:"./code/admin_equipe/update_equipe_niveau.php", 
										type		:"POST",
										cache		:false,
										data		:{id_equipe_sdstt:id_equipe, id_niveau_championnat:id_etat_selectionne},
										dataType	:"json",
										error		: function(request,error){
													alert("Erreur : responseText: "+request.responseText);
													},
										success		: function(reponse)
													{
														//console.log(reponse);
														if(reponse.resultat ==  tab_msg['code_ok']['id'])
														{
														}
														else
														{
															var res = reponse.resultat;
															var message = tab_msg[res]['texte'];
															$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
															$('#toast_enregistrement_echec').toast('show');
														}
													}
								});
	});
$('#admin_equipe').on("change",".select_poule",function(e){
	acqusition_echange();
	var id_etat_selectionne = $(e.currentTarget).val();
	//var id_colonne = $(e.currentTarget).parent().attr("id");
	var id_equipe = $(e.currentTarget).closest('.ligne_niveau_poule').attr("id");
	////console.log("id colonne (date):"+id_colonne);
	//console.log("id ligne:"+id_equipe);
	//console.log("id etat retenu :"+id_etat_selectionne);
							//mise � jour de l'�tat du joueur dans la base imm�diatement
							$.ajax({
									url			:"./code/admin_equipe/update_equipe_poule.php", 
									type		:"POST",
									cache		:false,
									data		:{id_equipe_sdstt:id_equipe, id_poule:id_etat_selectionne},
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse)
												{
													//console.log(reponse);
													if(reponse.resultat ==  tab_msg['code_ok']['id'])
													{
													}
													else
													{
														var res = reponse.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
													}
												}
							});
});
$('#admin_equipe').on("click",".suppression_equipe",function(e){
	acqusition_echange()
	$(this).blur();
	var id_equipe = $(e.currentTarget).closest('.ligne_niveau_poule').attr("id");
	console.log(id_equipe);
	$.ajax({
									url			:"./code/admin_equipe/suppression_equipe.php", 
									type		:"POST",
									cache		:false,
									data		:{id_equipe_sdstt:id_equipe},
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse)
												{
													console.log(reponse);
													if(reponse.resultat ==  tab_msg['code_ok']['id'])
													{
													
													}
													else
													{
														var res = reponse.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
													}
												}
							});
});
$('#ajout_equipe').on("click",function(e){
	$('#modal_ajout_equipe').modal('show');
	$('#modal_ajout_equipe_divers').val('');
	$('#modal_ajout_equipe_nom').val('');
	
});



$('#modal_ajout_equipe_button').on("click",function(e){
var nom_equipe = $('#modal_ajout_equipe_nom').val();
var divers_equipe = $('#modal_ajout_equipe_divers').val();
console.log(nom_equipe+'   - '+divers_equipe)
			$.ajax({
									url			:"./code/admin_equipe/ajout_equipe.php", 
									type		:"POST",
									cache		:false,
									data		:{nom : nom_equipe, divers:divers_equipe},
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse)
												{
													console.log(reponse);
													if(reponse.resultat ==  tab_msg['code_ok']['id'])
													{
													$('#modal_ajout_equipe').modal('hide');
													$('#toast_enregistrement_ok').addClass("show");
													}
													else
													{
														var res = reponse.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
													}
												}
							});
});