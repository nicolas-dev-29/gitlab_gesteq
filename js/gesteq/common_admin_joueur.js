	function initialisation_modal_ajout_joueurs(fonction_callback)
	{
	$('#modal_ajout_joueurs_nom').val("");
	$('#modal_ajout_joueurs_divers').val("");
	$('#modal_ajout_joueurs_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_joueurs_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_joueurs_nom_aide').show();
	$('#modal_ajout_joueurs_button').attr('disabled',false);
	$('#modal_ajout_joueurs_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}