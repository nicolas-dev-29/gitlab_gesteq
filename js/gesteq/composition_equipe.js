//$(function(){
var tab_msg={};
var tab_equipe_place_libre = [];
var nbre_joueur_equipe = 3;
var ligne_equipe_menu_drop='';
var nbre_joueurs_disponibles = {};
var nbre_joueurs_presents = {};
function acqusition_echange()
{
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				});
}
var tab_provisoire={};
tab_provisoire['id_date']="";
var tab_provisoire_initiale={};
tab_provisoire_initiale['id_date']="";
//
//permet de gérer le multi modal
//
$('body').on('hidden.bs.modal', function () {
if($('.modal.show').length > 0)
{
    $('body').addClass('modal-open');
}
});
//
/************************************************************************************************************/
//	chargement de la page
// 			chargement des donnees
/************************************************************************************************************/
	$(document).ready(function(){
			//masque des toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			
			
			
			$('#grille_equipes').empty();
			//masque des spinners
			//$.getJSON('./constantes/code_message.json',function(data){
			$.ajax({
				url		:"./constantes/code_message.json",
				Type	:"GET",
				cache	:false,
				dataType:"json",
				error		: function(request,error){
											alert("Erreur : responseText: "+request.responseText);
											},
				success		:function(data)
							{
							var tab_msg={};
							$.each(data,function(index,d){			
								var tab_msg_tampon={};
								tab_msg_tampon['id']= d.id;
								tab_msg_tampon['nom']=d.nom;
								tab_msg_tampon['texte']=d.texte;
								tab_msg[index]=tab_msg_tampon;
								});
							//////console.log(tab_msg);
								//rcupration de la liste des  journes
								$.ajax({
											url			:"./code/composition_equipe/liste_date.php", 
											type		:"POST",
											cache		:false,
											dataType	:"json",
											error		: function(request,error){
														alert("Erreur : responseText: "+request.responseText);
														},
											success		: function(reponse)
														{
															////console.log(reponse);
															var ligne_select='';
															$.each(reponse,function(i,item){
															nbre_joueurs_disponibles[i]=0;
															nbre_joueurs_presents[i]=0;
															////console.log(item.resultat);
															if(item.resultat ==  tab_msg['code_ok']['id'])
															{
																ligne_select +='<option value='+item.id+'> J'+item.journee+' / '+item.date+'</option>';
															}
															else
															{
																alert("erreur");
																var res = item.resultat;
																var message = tab_msg[res]['texte'];
																$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																$('#toast_enregistrement_echec').toast('show');
															}
															});
															$('#composition_equipe_date_liste_select').append(ligne_select);
														}
									});
							}
				});
		});
	$('#composition_equipe_date_liste_select').on('change',function(){
		var id_recup = $('#composition_equipe_date_liste_select').val();
		$('#encadre_nbre_presents').removeClass('element_cache_debut');
		var nbre_presents=0;
		nbre_joueurs_presents[id_recup] = 0;
		nbre_joueurs_disponibles[id_recup] = 0;
		tab_provisoire['id_date'] = id_recup;
		$('#grille_equipes').empty();
		$('#composition_equipe_liste_joueurs_disponible').empty();
		//////console.log(id_recup);
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				$.ajax({
					url			:"code/composition_equipe/liste_valeur_toutes.php", 
					type		:"POST",
					data		:{id:	id_recup},
					cache		:false,
					dataType	:"json",
					error		: function(request,error){
								alert("Erreur : responseText: "+request.responseText);
								},
					success		: function(reponse)
								{
									tab_provisoire['date_nom'] = reponse[0].nom_date;
									tab_provisoire['date_phase'] = reponse[0].phase_date;
									tab_provisoire['date_journee'] = reponse[0].journee_date;
									$('#composition_equipe_date_phase').text(tab_provisoire['date_phase']);
									$('#composition_equipe_date_journee').text(tab_provisoire['date_journee']);
									$('#composition_equipe_date_nom').text(tab_provisoire['date_nom']);
									console.log(reponse);
									var card='';
									var adversaire='';
									var numero_equipe='';
									var dom_ext='';	
									var nom_horaire='';
										$.each(reponse,function(i,item){
										if(item.resultat ==  tab_msg['code_ok']['id'])
										{
											  if(item.nom_adversaire == "EXEMPT")
											  {
												adversaire = "EXEMPT";
												numero_equipe="";
												dom_ext = "";
												nom_horaire = "";
											  }
											  else
											  {
												adversaire = item.nom_adversaire+" -";
												numero_equipe=item.num_equipe;
												dom_ext = item.nom_domext;
												nom_horaire = item.horaire_domext;

											  }
											  
											  card+="<div id="+item.id_equipe+" class=\"card\">\
														<div class=\"card-header\">\
															<div class=\"row\">\
																<div class=\"col-sm-3\">"+item.nom_equipe+"</div>\
																<div class=\"col-sm-5\"> division: "+item.nom_niveau+"</div>\
																<div class=\"col-sm-4\"> poule: "+item.nom_poule+"</div>\
															</div>\
														</div>\
														<div class=\"card-body\">\
															<div class=\"row justify-content-md-center mb-3\">\
																<div class=\"col-lg-auto text-center\">"+adversaire+" "+numero_equipe+"</div>\
																<div class=\"col-lg-auto text-center\">"+dom_ext+"</div>\
																<div class=\"col-lg-auto text-center\">"+nom_horaire+"</div>\
															</div>\
															<ul class=\"list-group \">\
															  <li class=\"list-group-item joueur_equipe row d-flex justify-contenst-between align-items-center\">\
																<div class=\"place_libre\">\
																<span>place libre 1</span>\
																<span class=\"badge element_cache_debut badge-primary badge-pill\">?</span>\
																</div>\
															  </li>\
															  <li class=\"list-group-item row joueur_equipe d-flex justify-content-between align-items-center\">\
																<div class=\"place_libre\">\
																<span>place libre 2</span>\
																<span class=\"badge element_cache_debut badge-primary badge-pill\">?</span>\
																</div>\
															  </li>\
															  <li class=\"list-group-item joueur_equipe row d-flex justify-content-between align-items-center\">\
																<div class=\"place_libre\">\
																<span>place libre 3</span>\
																<span class=\"badge element_cache_debut badge-primary badge-pill\">?</span>\
																</div>\
															  </li>\
															</ul>\
														 </div>\
													</div>";
										}
										else
										{
											var res = item.resultat;
											//var message = tab_msg[res]['texte'];
											$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
											$('#toast_enregistrement_echec').toast('show');
										}
										});
										$('#grille_equipes').append(card);
										/*******************************/
										//chargement des joueurs non disponibles
										/*******************************/
										$('#liste_absents').empty();
										$('#liste_non_definis').empty();
										$('#liste_oui_necessaire').empty();
										var ligne_absents ='';
										var nombre_absents=0;
										var ligne_non_definis ='';
										var nombre_non_definis=0;
										var ligne_oui_necessaire ='';
										var nombre_oui_necessaire=0;
										$.ajax({
																	url			:"code/composition_equipe/liste_joueurs_non_disponibles.php", 
																	type		:"POST",
																	data		:{id:	id_recup},
																	cache		:false,
																	dataType	:"json",
																	error		: function(request,error){
																				alert("Erreur : responseText: "+request.responseText);
																				},
																	success		: function(reponse)
																				{
																				console.log(reponse);
																				$.each(reponse,function(i,item){
																				switch(item.etat_joueur_initial)
																				{
																					case "1":	//non défini
																						$('#liste_non_definis').append(" "+item.nom_joueur+" "+item.prenom_joueur+" -")
																						nombre_non_definis++;
																					break;
																					case "3":	//absent
																						$("#liste_absents").append(" "+item.nom_joueur+" "+item.prenom_joueur+" -");
																						nombre_absents++;
																					break;
																					case "4":	//oui si nécessaire
																						$('#liste_oui_necessaire').append(" "+item.nom_joueur+" "+item.prenom_joueur+" -");
																						nombre_oui_necessaire++;
																						break;
																				}
																				$('#nbre_absents').text(nombre_absents);
																				$('#nbre_non_definis').text(nombre_non_definis);
																				$('#nbre_oui_necessaire').text(nombre_oui_necessaire);
																				});
																				console.log(ligne_absents);
																				//$('#liste_absents').text(ligne_absents);
																				}
												});
										/*******************************/
										//chargement des joueurs libres
										/*******************************/
										var ligne_equipe_select = '';
										var ligne_liste_joueur_libre = '';
										$('#composition_equipe_liste_joueurs_disponible').empty();
										//var ligne_equipe_menu_drop='';
										$.ajax({
												url			:"code/composition_equipe/liste_equipe.php", 
												type		:"POST",
												data		:{id:	id_recup},
												cache		:false,
												dataType	:"json",
												error		: function(request,error){
															alert("Erreur : responseText: "+request.responseText);
															},
												success		: function(reponse)
															{
															ligne_equipe_select+=	'<div class=" col-sm-6 justify-content-end my-2">\
																						<select class="composition_equipe_equipe_liste_select custom-select ">\
																							<option selected disabled value="-1"> choix équipe </option>';
															$.each(reponse,function(i,item){
																if(item.resultat ==  tab_msg['code_ok']['id'])
																{
																ligne_equipe_select+='<option value='+item.id+'>'+item.nom_equipe+'</option>';
																ligne_equipe_menu_drop+='<button class="dropdown-item boutons_joueur_transfert_equipe" id=' +item.id+ ' type="button">'+item.nom_equipe+'</button>';
																//initialisation du nombre de places libre par équipes
																tab_equipe_place_libre[item.id] = nbre_joueur_equipe;
																}															
																else
																{
																	var res = item.resultat;
																	var message = tab_msg[res]['texte'];
																	$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																	$('#toast_enregistrement_echec').toast('show');
																}
															});
															ligne_equipe_select+='</select></div></div>';
															$.ajax({
																	url			:"code/composition_equipe/liste_joueurs_disponibles.php", 
																	type		:"POST",
																	data		:{id:	id_recup},
																	cache		:false,
																	dataType	:"json",
																	error		: function(request,error){
																				alert("Erreur : responseText: "+request.responseText);
																				},
																	success		: function(reponse)
																				{
																				console.log(reponse);
																				$('#composition_equipe_liste_joueurs_disponible').text('');
																				$.each(reponse,function(i,item){
																				
																				if(item.resultat ==  tab_msg['code_ok']['id'])
																					{	
																						if((item.etat_joueur_initial == 2)&&(item.etat_joueur_final!=10)) //on supprime les doublons
																						{
																							nbre_joueurs_presents[id_recup]++;
																						}
																						if(item.etat_joueur_final == 10)
																						{
																						
																						nbre_joueurs_disponibles[id_recup]--;
																						ligne_liste_joueur_libre +='<div class="row ligne_joueur element_cache_debut" id="'+item.id_joueur+'">\
																														<div class="col-sm-6  justify-content-start">'+item.nom_joueur+' '+item.prenom_joueur+'\
																														<span class="badge badge-primary badge-pill">'+item.classement+'</span>\
																													</div>';
																						ligne_liste_joueur_libre+=ligne_equipe_select;
																						}
																						else
																						{	nbre_joueurs_disponibles[id_recup]++;
																							if(item.etat_joueur_final ==1)
																							{
																							ligne_liste_joueur_libre +='<div class="row ligne_joueur " id="'+item.id_joueur+'">\
																															<div class="col-sm-6  justify-content-start">'+item.nom_joueur+' '+item.prenom_joueur+'\
																															<span class="badge badge-primary badge-pill">'+item.classement+'</span>\
																														</div>';
																							ligne_liste_joueur_libre+=ligne_equipe_select;
																						}
																						}
																					}															
																					else
																					{
																						var res = item.resultat;
																						$('#toast_enregistrement_echec').toast('show');
																					}
																				});
																				$('#composition_equipe_liste_joueurs_disponible').append(ligne_liste_joueur_libre);
																				console.log("presents");
																				console.log(nbre_joueurs_presents[id_recup]);
																				$('#nbre_presents').text(nbre_joueurs_presents[id_recup]);
																				$('#nbre_disponibles').text(nbre_joueurs_disponibles[id_recup]);
																				}
																	});
															//repartition des joueurs	
															$.ajax({
																	url			:"code/composition_equipe/repartition_joueur.php", 
																	type		:"POST",
																	data		:{id:	id_recup},
																	cache		:false,
																	dataType	:"json",
																	error		: function(request,error){
																				alert("Erreur : responseText: "+request.responseText);
																				},
																	success		: function(reponse)
																				{
																				//console.log(reponse);
																				$.each(reponse,function(i,item){
																					if(item.resultat ==  tab_msg['code_ok']['id'])
																					{	
																						if((item.etat_joueur_initial == 2) && (item.etat_joueur_final == 10))
																						{ // on replace le joeuur dans les équipes.
																						console.log("ee"+item.equipe_sdstt);
																						tab_equipe_place_libre[item.equipe_sdstt]=tab_equipe_place_libre[item.equipe_sdstt]-1;
																						if(item.equipe_sdstt < 10)
																									{
																										var index_equipe_selection = "#\\3"+item.equipe_sdstt;
																									}
																									else
																									{
																										//transformer 10 en ascii
																									}
																						$(index_equipe_selection).find(".place_libre").first().replaceWith('<div class=" row place_occuppee col-sm-12" id='+item.id_joueur+ ' ">\
																														<div class="col-6"><span>'+item.nom_joueur +' '+item.prenom_joueur+'</span></div>\
																														<div class="col-2"><span class= "badge  badge-primary badge-pill ">'+item.classement+'</span> </div>\
																														<div id="boutons_joueur" class=" btn-toolbar col-4 text-right">\
																															 <button type="button" class="btn btn-secondary dropdown-toggle mr-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class= "fa fa-random fa-1x  "></span></button>\
																															  <div class="dropdown-menu" >'+ligne_equipe_menu_drop+'\</div>\
																															<button class="btn btn-danger suppression_joueur " name= "suppression_joueur " title= "suppression du joueur " data-toggle= "tooltip " data-placement= "top "   value= "suppression_evenement "> <span class= "fa fa-trash fa-1x  "></span></button>\
																														</div>\
																														</div>');
																						}
																					}															
																					else
																					{
																						var res = item.resultat;
																						$('#toast_enregistrement_echec').toast('show');
																					}
																				});
																				}
																	});
															}
											});
								}
						});
				});
	});
/************************************************************************************************************/
// appui sur un bouton de transfert
/************************************************************************************************************/
	$('#grille_equipes').on("click",".boutons_joueur_transfert_equipe",function(e){
	//console.log($(e.currentTarget));
	var id_recup = $('#composition_equipe_date_liste_select').val();
	var id_equipe_destination = $(e.currentTarget).attr('id');
	var id_equipe_depart = $(e.currentTarget).closest('.card').prop('id');
	var id_joueur_current = $(e.currentTarget).closest('.place_occuppee').prop('id');
	//console.log('date:'+id_recup+'joueur:'+id_joueur_current+' / equipe dep:'+id_equipe_depart+' / equipe dest:'+ id_equipe_destination+'-');
	if(tab_equipe_place_libre[id_equipe_destination]>0)
	{
	$.ajax({
			url			:"code/composition_equipe/update_transfert.php", 
			type		:"POST",
			data		:{id_date:	id_recup, id_equipe_dest: id_equipe_destination, id_equipe_dep:id_equipe_depart,id_joueur: id_joueur_current},
			cache		:false,
			dataType	:"json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse)
						{
							//console.log(reponse);
							tab_equipe_place_libre[id_equipe_depart]=tab_equipe_place_libre[id_equipe_depart]+1;
							tab_equipe_place_libre[id_equipe_destination]=tab_equipe_place_libre[id_equipe_destination]-1;
							$(e.currentTarget).closest('.place_occuppee').replaceWith('	<div class=\"place_libre\">\
																								<span>place libre </span>\
																								<span class=\"badge element_cache_debut badge-primary badge-pill\">?</span>\
																								</div>');
						if(id_equipe_destination < 10)
						{
							var index_equipe_selection = "#\\3"+id_equipe_destination;
						}
						else
						{
							//transformer 10 en ascii
						}
						$.ajax({
								url			:"code/composition_equipe/identite_joueur.php", 
								type		:"POST",
								data		:{id:	 id_joueur_current },
								cache		:false,
								dataType	:"json",
								error		: function(request,error){
											alert("Erreur : responseText: "+request.responseText);
											},
								success		: function(reponse)
											{
											//console.log(reponse);
											//le joueur disparait de la liste
											$(index_equipe_selection).find(".place_libre").first().replaceWith('<div class=" row place_occuppee col-sm-12" id='+reponse.id+ ' ">\
																														<div class="col-6"><span>'+reponse.nom +' '+reponse.prenom+'</span></div>\
																														<div class="col-2"><span class= "badge  badge-primary badge-pill ">'+reponse.classement+'</span> </div>\
																														<div id="boutons_joueur" class=" btn-toolbar col-4 text-right">\
																															 <button type="button" class="btn btn-secondary dropdown-toggle mr-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class= "fa fa-random fa-1x  "></span></button>\
																															  <div class="dropdown-menu" >'+ligne_equipe_menu_drop+'\</div>\
																															<button class="btn btn-danger suppression_joueur " name= "suppression_joueur " title= "suppression du joueur " data-toggle= "tooltip " data-placement= "top "   value= "suppression_evenement "> <span class= "fa fa-trash fa-1x  "></span></button>\
																														</div>\
																														</div>');
											}
								});																
						}
	});
	}
	else
	{
		// l'équipe est pleine
	$('#toast_enregistrement_echec_texte').text("plus de places disponibles");	
	$('#toast_enregistrement_echec').toast('show');
	}
	});
/************************************************************************************************************/
//	Select des joueurs libres
/************************************************************************************************************/
	$('#composition_equipe_liste_joueurs_disponible').on("change",".composition_equipe_equipe_liste_select",function(e){
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
	//	
	acqusition_echange();
		nbre_joueurs_disponibles[id_recup_date]--;
		$('#nbre_disponibles').text(nbre_joueurs_disponibles[id_recup_date]);
	
		var id_equipe_selectionne = $(e.currentTarget).val();
		var id_joueur_selectionne = $(e.currentTarget).parent().parent().attr("id");
		var id_ligne = $(e.currentTarget).closest(".ligne_joueur").attr("id");
		if(tab_equipe_place_libre[id_equipe_selectionne]>0)
			{
				//////console.log("equipe "+id_equipe_selectionne +" joueur "+id_joueur_selectionne+" ligne "+id_ligne+"/");
				$.ajax({
						url			:"code/composition_equipe/maj_joueur_equipe.php", 
						type		:"POST",
						data		:{id_date:	id_recup_date, id_joueur: id_joueur_selectionne, id_equipe: id_equipe_selectionne },
						cache		:false,
						dataType	:"json",
						error		: function(request,error){
									alert("Erreur : responseText: "+request.responseText);
									},
						success		: function(reponse)
									{
									//////console.log(reponse);
									//on l'ajoute à la liste des joueurs dans les équipes
									if(id_equipe_selectionne < 10)
									{
										var index_equipe_selection = "#\\3"+id_equipe_selectionne;
									}
									else
									{
										//transformer 10 en ascii
									}
									//////console.log($(index_equipe_selection).find(".place_libre").first().html(text_joueur));
									$ligne_joueur = '';
									$.ajax({
										url			:"code/composition_equipe/identite_joueur.php", 
										type		:"POST",
										data		:{id:	 id_joueur_selectionne },
										cache		:false,
										dataType	:"json",
										error		: function(request,error){
													alert("Erreur : responseText: "+request.responseText);
													},
										success		: function(reponse)
													{
													////console.log(reponse);
													//le joueur disparait de la liste
													$(e.currentTarget).parent().parent().fadeOut("fast");
													tab_equipe_place_libre[id_equipe_selectionne]=tab_equipe_place_libre[id_equipe_selectionne]-1;
													if(tab_equipe_place_libre[id_equipe_selectionne] < 0)
													{
													}
													$(index_equipe_selection).find(".place_libre").first().replaceWith('<div class=" row place_occuppee col-sm-12" id=' +reponse.id+ ' " >\
																														<div class="col-6"><span>'+reponse.nom +' '+reponse.prenom+'</span></div>\
																														<div class="col-2"><span class= "badge  badge-primary badge-pill ">'+reponse.classement+'</span> </div>\
																														<div id="boutons_joueur" class=" btn-toolbar col-4 text-right">\
																															 <button type="button" class="btn btn-secondary dropdown-toggle mr-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class= "fa fa-random fa-1x  "></span></button>\
																															  <div class="dropdown-menu" >'+ligne_equipe_menu_drop+'\</div>\
																															<button class="btn btn-danger suppression_joueur " name= "suppression_joueur " title= "suppression du joueur " data-toggle= "tooltip " data-placement= "top "   value= "suppression_evenement "> <span class= "fa fa-trash fa-1x  "></span></button>\
																														</div>\
																														</div>');
													}
										});
									}
				});
			}
			else
			{
			// l'équipe est pleine
			$('#toast_enregistrement_echec_texte').text("plus de places disponibles");	
			$('#toast_enregistrement_echec').toast('show');
			}
	});
	$('#grille_equipes').on("click",".suppression_joueur",function(e){
	$(this).blur();
	////console.log($(e.currentTarget).parent().parent().attr('id'));
	
	var id_joueur_selectionne = $(e.currentTarget).parent().parent().attr('id');
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
	var id_equipe_selectionne = $(e.currentTarget).parent().parent().parent().parent().parent().parent().attr('id');
	
	nbre_joueurs_disponibles[id_recup_date]++;
	$('#nbre_disponibles').text(nbre_joueurs_disponibles[id_recup_date]);
	$.ajax({
			url			:"code/composition_equipe/suppression_joueur.php", 
			type		:"POST",
			data		:{id_date:	id_recup_date, id_joueur: id_joueur_selectionne},
			cache		:false,
			dataType	:"json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse)
						{
						console.log(reponse);
						tab_equipe_place_libre[id_equipe_selectionne]=tab_equipe_place_libre[id_equipe_selectionne]+1;
						//remise en place libre
						if(id_equipe_selectionne < 10)
						{
							var index_equipe_selection = "#\\3"+id_equipe_selectionne;
						}
						else
						{
							//transformer 10 en ascii
						}
						$(e.currentTarget).parent().parent().replaceWith('	<div class=\"place_libre\">\
																								<span>place libre </span>\
																								<span class=\"badge element_cache_debut badge-primary badge-pill\">?</span>\
																								</div>');
						$(".ligne_joueur").each(function(i,item){
						if(item.id == parseInt(id_joueur_selectionne))
						{
							$(this).removeClass("element_cache_debut");
							$(this).fadeIn("fast");
							$(this).find('select').val(-1);
						}
						else
						{
						}
						});
						}
			});
	});
/************************************************************************************************************/
//	bandeaux des boutons
// 			actions des boutons enregistrer / raz / quitter / envoi mail aux joueurs
/************************************************************************************************************/
//
/************************************************************************************************************/
//		envoi mail aux joueurs
/************************************************************************************************************/
$('#composition_equipe_envoi_mail_joueurs').on('click',function(e){
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
	//ouverture du modal d'envoi
	$('#modal_composition_equipe_mail').modal('show');
	//}
});
$('#modal_composition_equipe_mail').on('show.bs.modal',function(e){
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
	var ligne='';
	$('#modal_composition_equipe_liste').empty();
	var ligne_rendez_vous = '';
	$.ajax({
				url			:"code/composition_equipe/liste_rendez_vous.php", 
				type		:"POST",
				data		:{id:	id_recup_date},
				cache		:false,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse)
							{
							//console.log(reponse);
							ligne_rendez_vous='<select  class=\"custom-select select_lieu_rendez_vous\">';
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
								ligne_rendez_vous+='<option value='+item.id+'>'+item.nom_rendez_vous+'</option>';
								}															
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
							ligne_rendez_vous+="</select>";
								$.ajax({
												url			:"code/composition_equipe/liste_valeur_toutes.php", 
												type		:"POST",
												data		:{id:	id_recup_date},
												cache		:false,
												dataType	:"json",
												error		: function(request,error){
															alert("Erreur : responseText: "+request.responseText);
															},
												success		: function(reponse)
															{
															console.log(reponse);
															var ligne_date ='phase '+reponse[0].phase_date+' - journée n° '+reponse[0].journee_date+' - '+reponse[0].nom_date+' ';
															$('#modal_composition_equipe_date').html(ligne_date);
															var ligne = '';	
															
															$.each(reponse,function(i,item){
															if(item.resultat ==  tab_msg['code_ok']['id'])
															{
															ligne += "	<div class=\"ligne_equipe\" id="+item.id_equipe+"\>\
																			<div class=\"row\">\
																				<h5><div class=\"col-lg-auto\">"+item.nom_equipe+"</div></h5>\
																				<div class=\"col-lg-auto\">  "+item.nom_niveau+"</div>\
																				<div class=\"col-lg-auto\">"+item.nom_poule+"</div>\
																				<div class=\"col-lg-auto text-center\">"+item.nom_adversaire+" - "+item.num_equipe+"</div>\
																			</div>\
																			<div class=\"row\">\
																				<div class=\"col-lg-2 text-center\"> à "+item.nom_domext+"</div>\
																				<div class=\"col-lg-2 text-center\">"+item.horaire_domext+"</div>\
																				<div class=\"col-lg-4 text-center\"> rendez-vous:"+ligne_rendez_vous+"</div>\
																				<div class=\"col-lg-4 text-center\">horaire:<input type=\"time\" class=\"form-control text-center select_horaire_rendez_vous\" ></div>\
																			</div>\
																		</div>";
																	}
																	else
																	{
																		var res = item.resultat;
																		//var message = tab_msg[res]['texte'];
																		$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																		$('#toast_enregistrement_echec').toast('show');
																	}
																});
															$('#modal_composition_equipe_liste').append(ligne);
															}
										});
									}
					});
});
//sauvegarde des modifications
$('#modal_composition_equipe_liste').on("change",".select_lieu_rendez_vous",function(e){
	var id_lieu_selectionne = $(e.currentTarget).val();
	var id_equipe_selectionne = $(e.currentTarget).closest('.ligne_equipe').attr("id");
	//console.log("lieu: "+id_lieu_selectionne+" / equipe"+id_equipe_selectionne);
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
		$.ajax({
			url			:"code/composition_equipe/update_lieux_rendez_vous.php", 
			type		:"POST",
			data		:{id_date:id_recup_date, id_equipe: id_equipe_selectionne, id_lieu:id_lieu_selectionne},
			cache		:false,
			dataType	:"json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse)
						{
							//console.log(reponse);
						}
			});
});
$('#modal_composition_equipe_liste').on("change",".select_horaire_rendez_vous",function(e){
	var horaire_selectionne = $(e.currentTarget).val()+":00";
	var id_equipe_selectionne = $(e.currentTarget).closest('.ligne_equipe').attr("id");
	//console.log("horaire: "+horaire_selectionne+" / equipe"+id_equipe_selectionne);
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
		$.ajax({
			url			:"code/composition_equipe/update_horaire_rendez_vous.php", 
			type		:"POST",
			data		:{id_date:id_recup_date, id_equipe: id_equipe_selectionne, horaire:horaire_selectionne},
			cache		:false,
			dataType	:"json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse)
						{
							//console.log(reponse);
						}
			});
});
/************************************************************************************************************/
// bouton envoi du mail aux joueurs
/************************************************************************************************************/
$('#modal_envoi_mail_button').on('click',function(e){
	$('#modal_composition_equipe_mail').modal('hide');
	$('#contenu_message_coach').val("");
	$('#modal_composition_equipe_mail_message_coach').modal('show');
	$(this).focus();
	});
$('#modal_envoi_mail_button_message_coach').on('click',function(e){
//récupération du message du coach
	var contenu_message_coach_recup = $("#contenu_message_coach").val();
//récupération des informations
	$('#modal_composition_equipe_mail_message_coach').modal('hide');
	$('#modal_composition_equipe_mail_previsualisation').modal('show');
	//$('#modal_composition_equipe_mail').modal('hide');
	$('#contenu_mail').html("");
	var validation_mail=0;
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
	console.log("date:"+id_recup_date);
			$.ajax({
			url			:"code/composition_equipe/creation_mail.php", 
			type		:"POST",
			data		:{id:id_recup_date , validation:validation_mail , message_coach: contenu_message_coach_recup},
			cache		:false,
			dataType	:"json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse)
						{
							console.log(reponse);
							$('#contenu_mail').html(reponse.mail);
						}
			});
});
//envoi des informations vers le script PHP d'envoi de mail
$('#modal_envoi_mail_button_validation').on('click',function(e){
	//récupération des informations
	$('#modal_composition_equipe_mail_previsualisation').modal('hide');
	$('#modal_composition_equipe_mail').modal('hide');
	$('#contenu_mail').html("");
	var contenu_message_coach_recup = $("#contenu_message_coach").val();
	var validation_mail=1;
	var id_recup_date = $('#composition_equipe_date_liste_select').val();
	//console.log("date:"+id_recup_date);
			$.ajax({
			url			:"code/composition_equipe/creation_mail.php", 
			type		:"POST",
			data		:{id:id_recup_date , validation:validation_mail , message_coach: contenu_message_coach_recup},
			cache		:false,
			dataType	:"json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse)
						{
							//console.log(reponse);
							$('#contenu_mail').html(reponse.mail);
						}
			});
});
//});