var tab_provisoire={};
var tab_provisoire_initiale={};
var tab_msg={};
var tab_etat_initial_joueur=[];
var max_num_equipe_adverse = 12;
function acqusition_echange()
{
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				});
}
/************************************************************************************************************/
//	chargement de la page
// 			chargement des donnees
/************************************************************************************************************/
		$(function(){
			//masque des toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			$('#table_grille_presence_entete').empty();
			$('#table_grille_presence_ligne').empty();
			//masque des spinners
			$.ajax({
				url		:"./constantes/code_message.json",
				Type	:"GET",
				cache	:false,
				dataType:"json",
				error		: function(request,error){
											alert("Erreur : responseText: "+request.responseText);
											},
				success		:function(data)
							{
							var tab_msg={};
							$.each(data,function(index,d){			
								var tab_msg_tampon={};
								tab_msg_tampon['id']= d.id;
								tab_msg_tampon['nom']=d.nom;
								tab_msg_tampon['texte']=d.texte;
								tab_msg[index]=tab_msg_tampon;
								});
							////console.log(tab_msg);
							var ligne_select='';
							var ligne_form_nom='';
							var ligne_form_num_eq='';
							var ligne_form_domext='';
							ligne_form_num_eq+='<select  class="custom-select select_adversaires_num_eq">\
																			<option value="-1">   </option>';
							ligne_form_domext+='<input id="essai" class=" select_domext" type="checkbox" value=2> extérieur	'
							/*
							*/	
							for(var index = 1; index< max_num_equipe_adverse; index++)
							{
								if(index == 1)
								{
								ligne_form_num_eq +='<option  value='+index+' selected> '+index+' </option>';
								}
								else
								{
								ligne_form_num_eq +='<option  value='+index+'> '+index+' </option>';
								}
							}
							ligne_form_num_eq+='</select>'
							var index_tab=01;
							//récupration de la liste des états
								$.ajax({
											url			:"./code/admin_adversaires/admin_liste_clubs_a.php", 
											type		:"POST",
											cache		:false,
											dataType	:"json",
											error		: function(request,error){
														alert("Erreur : responseText: "+request.responseText);
														},
											success		: function(reponse)
														{
															ligne_form_nom+='<select  class="custom-select select_adversaires_nom">\
																			<option value="-1">   </option>'
															$.each(reponse,function(i,item){
															//console.log(item.resultat);
															if(item.resultat ==  tab_msg['code_ok']['id'])
															{
																ligne_form_nom +='<option  value='+item.id_club_A+'> '+item.nom_club_A+' </option>';
															}
															else
															{
																alert("erreur");
																var res = item.resultat;
																var message = tab_msg[res]['texte'];
																$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																$('#toast_enregistrement_echec').toast('show');
															}
															});
															ligne_form_nom+='</select>';
															$.ajax({
																	url			:"./code/composition_equipe/liste_date.php", 
																	type		:"POST",
																	cache		:false,
																	dataType	:"json",
																	error		: function(request,error){
																				alert("Erreur : responseText: "+request.responseText);
																				},
																	success		: function(reponse)
																				{
																					var ligne_entete='';
																					ligne_entete+='<th col=\"scope\">equipes SDSTT  </th>'
																					$.each(reponse,function(i,item){
																					////console.log(item.resultat);
																					if(item.resultat ==  tab_msg['code_ok']['id'])
																					{
																						ligne_entete +='<th col=\"scope\" id='+item.id+'> J'+item.journee+' / '+item.date+'</th>';
																						ligne_select +='<td class="select_adervsaire_full" id='+index_tab+'>nom:'+ligne_form_nom+' numéro:'+ligne_form_num_eq+ligne_form_domext+'</td>';
																						index_tab++;
																					}
																					else
																					{
																						alert("erreur");
																						var res = item.resultat;
																						var message = tab_msg[res]['texte'];
																						$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																						$('#toast_enregistrement_echec').toast('show');
																					}
																					});
																					$('#table_grille_adversaires_entete').append(ligne_entete);
																						//chargement des joueurs
																						$.ajax({
																							url			:"code/composition_equipe/liste_equipe.php", 
																							type		:"POST",
																							cache		:false,
																							dataType	:"json",
																							error		: function(request,error){
																										alert("Erreur : responseText: "+request.responseText);
																										},
																							success		: function(reponse)
																										{
																											////console.log(reponse);
																											$('#table_grille_presence_ligne tr').each(function(){
																												$(this).remove();
																											});
																											var ligne='';
																											var button_select;
																											$.each(reponse,function(i,item){
																											if(item.resultat ==  tab_msg['code_ok']['id'])
																											{
																												ligne+='<tr class="grille_adversaires_ligne " id='+item.id+'><td class="align-middle">'+ item.nom_equipe +'\
																												</td>'+ligne_select+'</tr>';
																											}
																											else
																											{
																												alert("erreur");
																												var res = item.resultat;
																												var message = tab_msg[res]['texte'];
																												$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																												$('#toast_enregistrement_echec').toast('show');
																											}
																											});
																											$('#table_grille_adversaires_ligne').append(ligne);
																												$.ajax({
																													url			:"./code/admin_adversaires/liste_adversaires.php", 
																													type		:"POST",
																													cache		:false,
																													dataType	:"json",
																													error		: function(request,error){
																																alert("Erreur : responseText: "+request.responseText);
																																},
																													success		: function(reponse)
																																{
																																console.log(reponse);
																																$(".select_adversaires_nom").each(function(i,index){
																																	//console.log("L:"+$(this).parent().prop("id")+"/C: "+$(this).parent().parent().prop("id"));
																																	var ligne = $(this).parent().parent().prop("id");
																																	var colonne = $(this).parent().prop("id");
																																	$(this).find('option[value="'+reponse[i]['id_adversaire']+'"]').prop('selected', true);
																																	//$(this).find('.select_adversaires_nom').val(+reponse[i]['id_adversaire']);
																																	});
																																$(".select_adversaires_num_eq").each(function(i,index){
																																	$(this).find('option[value="'+reponse[i]['num_equipe']+'"]').prop('selected', true);
																																	});
																																$(".select_domext").each(function(i,index){
																																	if(reponse[i]['domext'] == 2) //extérieur
																																	{
																																	$(this).prop('checked',true);
																																	}
																																	else
																																	{
																																	$(this).prop('checked',false);
																																	}
																																	});
																																}
																												});
																										}
																							});
																				}
																});
														}
									});			
							}
				});
				acqusition_echange();
//affichage des valeurs initiales
		});
//récupération de la valeur de select sur un changmement
//$('#table_grille_adversaires').on("change",".select_adversaires_nom",function(e){
$('#table_grille_adversaires').on("change",".select_adervsaire_full",function(e){
		acqusition_echange();
		var id_colonne = $(e.currentTarget).attr("id");
		var id_ligne = $(e.currentTarget).closest('tr').attr("id");
		var id_adversaire_selectionne = $(e.currentTarget).find('.select_adversaires_nom').val();
		var id_adversaire_num_equipe_selectionne = $(e.currentTarget).find('.select_adversaires_num_eq').val();
		var id_domext = 1;
		if($(e.currentTarget).find(':checkbox').is(':checked'))
		{
			//extérieur
			id_domext = 2;
		}
		else
		{
			id_domext = 1;
		}
		//
		//console.log("id colonne (date):"+id_colonne);
		//console.log("id ligne (équipe):"+id_ligne);
		//console.log("id adversaire retenu :"+id_adversaire_selectionne);
		//console.log("id adversaire num retenu :"+id_adversaire_num_equipe_selectionne);
		//console.log("id domext :"+id_domext);
								//mise à jour de l'état du joueur dans la base immédiatement
								$.ajax({
										url			:"./code/admin_adversaires/update_adversaires.php", 
										type		:"POST",
										cache		:false,
										data		:{id_date:id_colonne, id_equipe:id_ligne, id_adversaire:id_adversaire_selectionne, id_num_equipe:id_adversaire_num_equipe_selectionne, id_dom_ext:id_domext },
										dataType	:"json",
										error		: function(request,error){
													alert("Erreur : responseText: "+request.responseText);
													},
										success		: function(reponse)
													{
														//console.log(reponse);
														if(reponse.resultat ==  tab_msg['code_ok']['id'])
														{
														}
														else
														{
															var res = reponse.resultat;
															var message = tab_msg[res]['texte'];
															$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
															$('#toast_enregistrement_echec').toast('show');
														}
													}
								});
	});