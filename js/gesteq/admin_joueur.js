$(function(){
var pattern_joueurs_nom = /.{3,}/i; 	//tous les caract�res nbre:3->20 sans saut ligne
var pattern_joueurs_divers = /.{0,255}/i;
var longueur_max_nom = 20;
var longueur_max_divers = 255;
	
var tab_msg={};

$('[data-toggle="tooltip"]').tooltip();

/********************************************************/
// fonctions admin joueurs
/********************************************************/
/**variagles globales                                  **/
function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise � jour du tableau des joueurs
			$.ajax({
				url      	: "code/admin_joueurs/admin_joueurs_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								$('#nombre_joueurs').html("<b>"+reponse.length+"</b>");
								$('#table_joueurs tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_joueurs" name="modification_joueurs" 	data-toogle="tooltip" data-placement="top" title="modification du lieu"	value="modification_joueurs">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_joueurs" name="suppression_joueurs" title="suppression du lieu" data-toggle="tooltip" data-placement="top"   value="suppression_joueurs"><span class="fa fa-trash fa-1x "></span></button>';
								
								var ligne_table='';
								$.each(reponse,function(i,item){
								////console.log(item.resultat);
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										
										if(item.selection == 1)
										{
											var selection_joueur ='<input class=" custom-checkbox selection_joueurs" type="checkbox" value="" checked>'
										}
										else
										{
											var selection_joueur ='<input class=" custom-checkbox selection_joueurs" type="checkbox" value="" >'
										}
										ligne_table +='<tr class="admin_joueurs" id='+item.id+'><td>'+ item.nom + '</td><td>' + item.prenom+
										'</td><td>'+item.classement+ '</td><td>'+selection_joueur+
										 '</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
										
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_joueurs').append(ligne_table);
							}						
					});	
			});
}
	
	$(document).ready(function(){
		//alert("mise � jour des variables de la page");
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		
		$('#suppression_joueurs_tous_spinner').hide();
		$('#ajout_joueurs_spinner').hide();
		
		mise_a_jour_liste();  


		//r�cup�ration des informations d'�changes du fichier JSON (code erreur,..)
		
		
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				
				////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
		
	});
	
	/*************************************************/
	// modal suppression de tous les joueurs en une fois
	/************************************************/	
	//affichage du modal
	$('#suppression_joueurs_tous').click(function(event){
		$('#modal_suppression_joueurs_tous').modal('show');
	});
	//appui sur le bouton de suppression
	$('#modal_suppression_joueurs_tous_button').click(function(event){
		$('#modal_suppression_joueurs_tous').hide();
		$('#suppression_joueurs_tous_spinner').show();
		
		//suppression en cours
		//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/joueurs/admin_joueurs_suppression_tous.php",
			type   		: "POST",		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_joueurs_tous').modal('toggle');
							$('#suppression_joueurs_tous_spinner').hide();
							mise_a_jour_liste();
							
						 },
			success  	: function(retour_json) 
						{  
						$('#suppression_joueurs_tous_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']==tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_joueurs_tous').modal('toggle');
							mise_a_jour_liste();
							
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_joueurs_tous').modal('toggle');
							
							mise_a_jour_liste();
							}
						}													
				});		
		
		
	});
	
	
	/*************************************************/
	// modal modification des joueurs
	/************************************************/
	
	//s�lection des boutons "modifier" par la m�thode de d�l�gation car ils sont ajout�s dynamiquement
	$('#table_joueurs').on("click",".modification_joueurs",function(e){
		
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var prenom_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		var classement_recup = $(e.currentTarget).parent().parent().find("td").eq(02).html();
		//var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		console.log("id: "+id_recup +" / nom: "+nom_recup+" " );
		
		//ouverture modal de modification
		$('#modal_modification_joueurs_nom').val("");
		$('#modal_modification_joueurs_prenom').val("");
		$('#modal_modification_joueurs_mail_1').val("");
		$('#modal_modification_joueurs_mail_2').val("");
		$('#modal_modification_joueurs_tel_1').val("");
		$('#modal_modification_joueurs_tel_2').val("");
		
		
		$('#modal_modification_joueurs_classement').val("");
		$('#modal_modification_joueurs_divers').val("");
		$('#modal_modification_joueurs_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_prenom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_classement').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_divers').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_mail_1').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_mail_2').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_tel_1').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_tel_2').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_joueurs_nom').focus();
		
		$('#modal_modification_joueurs').modal('show');
		
		
		//$('#modal_modification_joueurs').on('shown.bs.modal', function() {
		$('#modal_modification_joueurs_nom').focus();
			//$('#modal_modification_joueurs_prenom').val(prenom_recup);	
			//$('#modal_modification_joueurs_nom').val(nom_recup);	
			//$('#modal_modification_joueurs_classement').val(classement_recup);	
			//mise en place des valeurs issues de la base
		$.ajax({
					 url      	: "code/admin_joueurs/admin_joueurs_informations.php",
					type   		: "POST",
					data     	: { id:id_recup},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_joueurs').modal('toggle');
								 },
					success  	: function(retour_json) 
								{
									console.log(retour_json);
									console.log("retouR "+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										$('#modal_modification_joueurs_nom').val(retour_json['nom']);
										$('#modal_modification_joueurs_prenom').val(retour_json['prenom']);
										$('#modal_modification_joueurs_mail_1').val(retour_json['mail_parent_1']);
										$('#modal_modification_joueurs_mail_2').val(retour_json['mail_parent_2']);
										$('#modal_modification_joueurs_tel_1').val(retour_json['tel_parent_1']);
										$('#modal_modification_joueurs_tel_2').val(retour_json['tel_parent_2']);
										$('#modal_modification_joueurs_classement').val(retour_json['classement']);
										$('#modal_modification_joueurs_divers').val(retour_json['divers']);
										
									}
								}
				});
										
			
			//});
			//$('#modal_modification_joueurs_divers').val(divers_recup);
			//$('#modal_modification_joueurs_id').attr("value",id_recup);
			
			
		}); //fin du On click	
			
		//remise � z�ro des couleurs avant l'entr�e des informations	
		$( "#modal_modification_joueurs_nom" ).focus(function() {
					$('#modal_modification_joueurs_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_joueurs_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_joueurs_nom_aide').show();
					
					$('#modal_modification_joueurs_prenom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_joueurs_prenom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_joueurs_prenom_aide').show();
					
					$('#modal_modification_joueurs_classement').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_joueurs_classement_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_joueurs_classement_aide').show();
					
					$('#modal_modification_joueurs_button').attr('disabled',false);
					$('#modal_modification_joueurs_button').removeClass('disabled');
		});
		
		//test de la validit� des donn�es
		$( "#modal_modification_joueurs_nom" ).blur(function() {
			
			//r�cup�ration de l'id de la ligne concern�e
			var id_recup= $('#modal_modification_joueurs_id').attr("value");
			
			

		
			//recherche d'un �l�ment d�j� pr�sent
			var nom_joueur = $('#modal_modification_joueurs_nom').val();
			var longueur_nom_joueur = nom_joueur.length;
			if($('#modal_modification_joueurs_nom').val()!="")
				{
				if(pattern_joueurs_nom.test(nom_joueur)&&(longueur_nom_joueur<longueur_max_nom))
				{
				//envoi des donn�es vers le fichier PHP de traitement en AJAX
				console.log(id_recup);
				$.ajax({
					 url      	: "code/admin_joueurs/admin_joueurs_modification_vérifications.php",
					type   		: "POST",
					data     	: {nom_joueur: nom_joueur, id:id_recup},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_joueurs').modal('toggle');
								 },
					success  	: function(retour_json) {
								//3 cas: 	- le nom n'est pas pr�sent ->ok
								//			- le nom est pr�sent mais c'est celui du modal (on modifie le champs divers) ->ok
								//			- sinon, �chec
									console.log("retour"+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										
										$('#modal_modification_joueurs_nom').addClass('is-valid');
										$('#modal_modification_joueurs_nom').removeClass('is-invalid');
										$('#modal_modification_joueurs_nom_label').addClass('text-success');
										$('#modal_modification_joueurs_erreur').text("le lieu est valide");
										$('#modal_modification_joueurs_button').attr('disabled',false);
										$('#modal_modification_joueurs_button').removeClass('disabled');
										$('#modal_modification_joueurs_nom_aide').hide();
									}
									else
									{
										$('#modal_modification_joueurs_nom').addClass('is-invalid');
										$('#modal_modification_joueurs_nom').removeClass('is-valid');
										$('#modal_modification_joueurs_nom_label').addClass('text-danger');
										$('#modal_modification_joueurs_nom_aide').hide();
										
										$('#modal_modification_joueurs_erreur').text("échec");					
										$('#modal_modification_joueurs_button').attr('disabled',true);
										$('#modal_modification_joueurs_button').addClass('disabled');
									}
								}
					
					});
				}
				else
				{
				// le nom ne passe pas le pattern
				$('#modal_modification_joueurs_nom').addClass('is-invalid');
				$('#modal_modification_joueurs_nom_label').addClass('text-danger');
				$('#modal_modification_joueurs_nom_aide').hide();
				
				$('#modal_modification_joueurs_erreur').text("le nom doit comporter entre 4 et 20 caract�res");					
				$('#modal_modification_joueurs_button').attr('disabled',true);
				$('#modal_modification_joueurs_button').addClass('disabled');
				}
			}
		else
		{
		$('#modal_modification_joueurs_nom').addClass('is-invalid');
		$('#modal_modification_joueurs_nom_label').addClass('text-danger');
		$('#modal_modification_joueurs_nom_aide').hide();
		
		$('#modal_modification_joueurs_erreur').text("le  champs est vide");					
		$('#modal_modification_joueurs_button').attr('disabled',true);
		$('#modal_modification_joueurs_button').addClass('disabled');
		}
	});
		
	$('#modal_modification_joueurs_divers').blur(function() {
		
		var divers_modifie = $('#modal_modification_joueurs_divers').val();
		var longueur_divers_modifie = divers_modifie.length;
		//test du pattern
		if(pattern_joueurs_divers.test(divers_modifie)&&(longueur_divers_modifie<longueur_max_divers))
		{
		//champs valide
			$('#modal_modification_joueurs_divers').removeClass('is-invalid');
			$('#modal_modification_joueurs_divers').addClass('is-valid');
			$('#modal_modification_joueurs_divers_label').removeClass('text-danger');
			$('#modal_modification_joueurs_divers_label').addClass('text-success');
			
			$('#modal_modification_joueurs_divers_erreur').text("le champs est valide!");
			$('#modal_modification_joueurs_button').attr('disabled',false);
			$('#modal_modification_joueurs_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_joueurs_divers').removeClass('is-valid');
			$('#modal_modification_joueurs_divers').addClass('is-invalid');
			$('#modal_modification_joueurs_divers_label').addClass('text-danger');
			$('#modal_modification_joueurs_divers_label').removeClass('text-success');
		
			$('#modal_modification_joueurs_nom_aide').hide();
			$('#modal_modification_joueurs_divers_erreur').text("le champs doit comporter 255 caract�res maximum");					
			$('#modal_modification_joueurs_button').attr('disabled',true);
			$('#modal_modification_joueurs_button').addClass('disabled');
		}
	
	});
	
	//mise � jour des donn�es depuis la fen�tre modal
	$('#modal_modification_joueurs_button').click(function(event){
		
		//r�cup�ration des informations de la fen�tre modal
		var nom_joueurs_modifie = $('#modal_modification_joueurs_nom').val();
		var prenom_joueurs_modifie = $('#modal_modification_joueurs_divers').val();
		var id_modifie = $('#modal_modification_joueurs_id').attr("value");
		
	var joueurs_mail_1_modifie = $('#modal_modification_joueurs_mail_1').val();
	var joueurs_mail_2_modifie = $('#modal_modification_joueurs_mail_2').val();
	var joueurs_tel_1_modifie = $('#modal_modification_joueurs_tel_1').val();
	var joueurs_tel_2_modifie = $('#modal_modification_joueurs_tel_2').val();
	var joueurs_classement_modifie = $('#modal_modification_joueurs_classement').val();
	var joueurs_divers_modifie = $('#modal_modification_joueurs_divers').val();
										
		
		
			//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/admin_joueurs/admin_joueurs_modification.php",
			type   		: "POST",
			data     	: {	id:id_modifie, nom:nom_joueurs_modifie, prenom:nom_joueurs_modifie,
							mail_1:joueurs_mail_1_modifie, mail_2:joueurs_mail_2_modifie, tel_1:joueurs_tel_1_modifie, tel_2:joueurs_tel_2_modifie,
							classement: joueurs_classement_modifie, divers: joueurs_divers_modifie },		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_joueurs').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_modification_joueurs').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_joueurs').modal('toggle');
							mise_a_jour_liste();
							
							}
						}													
				});
	});			
	/*************************************************/
	// modal suppression d'un joueur
	/************************************************/
	function initialisation_modal_suppresion_joueurs(fonction_callback)
	{
	$('#modal_suppression_joueurs_nom').val("");
	$('#modal_suppression_joueurs_divers').val("");
	$('#modal_suppression_joueurs_button').attr('disabled',false);
	$('#modal_suppression_joueurs_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	
	
	
	//s�lection des boutons "supprimer" par la m�thode de d�l�gation car ils sont ajout�s dynamiquement
	$('#table_joueurs').on("click",".suppression_joueurs",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		
	
		//ouverture modal de modification			
		initialisation_modal_ajout_joueurs(function(){
			$('#modal_suppression_joueurs_id').attr("value",id_recup);
			$('#modal_suppression_joueurs_nom ').text(nom_recup);	
			$('#modal_suppression_joueurs_divers').text(divers_recup);
			
			$('#modal_suppression_joueurs').modal('show'); 
			
			});
		}); //fin du On click	
		
		
			$('#modal_suppression_joueurs').on('shown.bs.modal', function() {		
			
			});			
		
		
	//mise � jour des donn�es depuis la fen�tre modal
	$('#modal_suppression_joueurs_button').click(function(event){
		
		//r�cup�ration des informations de la fen�tre moddal
		var lieu_modifie = $('#modal_suppression_joueurs_nom').text();
		var divers_modifie = $('#modal_suppression_joueurs_divers').text();
		var id_modifie = $('#modal_suppression_joueurs_id').attr("value");
		
		//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/joueurs/admin_joueurs_suppression.php",
			type   		: "POST",
			data     	: {lieu: lieu_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_joueurs').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_joueurs').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_joueurs').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});			
	
	/*************************************************/
	// modal ajout des joueurs
	/************************************************/
/*	
	function initialisation_modal_ajout_joueurs(fonction_callback)
	{
	$('#modal_ajout_joueurs_nom').val("");
	$('#modal_ajout_joueurs_divers').val("");
	$('#modal_ajout_joueurs_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_joueurs_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_joueurs_nom_aide').show();
	$('#modal_ajout_joueurs_button').attr('disabled',false);
	$('#modal_ajout_joueurs_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	 
*/	
	
	$('#ajout_joueurs').click(function() {
		initialisation_modal_ajout_joueurs(function(){
			$('#modal_ajout_joueurs').modal('show');
			$('#modal_ajout_joueurs_nom').focus()	
			});
		});
		
		$('#modal_ajout_joueurs').on('shown.bs.modal', function() {
			$('#modal_ajout_joueurs_nom').focus();
						});
		
		$('#modal_ajout_joueurs').on('hidden.bs.modal', function (e) {
//en r�serve		
		});
			
		
		//remise � z�ro des couleurs avant l'entr�e des informations	
		$( "#modal_ajout_joueurs_nom" ).focus(function() {
					$('#modal_ajout_joueurs_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_joueurs_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_joueurs_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_joueurs_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_joueurs_nom_aide').show();
					$('#modal_ajout_joueurs_button').attr('disabled',false);
					$('#modal_ajout_joueurs_button').removeClass('disabled');
				
			});
		
		//test de la validit� des donn�es
		$( "#modal_ajout_joueurs_nom" ).blur(function() {
			
			//recherche d'un �l�ment d�j� pr�sent
			var lieu_ajoute = $('#modal_ajout_joueurs_nom').val();
			var longueur_lieu_ajoute = lieu_ajoute.length;
			
			
			if(($('#modal_ajout_joueurs_nom').val()!=""))
			{	
				//test du pattern
				if(pattern_joueurs_nom.test(lieu_ajoute)&&(longueur_lieu_ajoute<longueur_max_nom))
				{
				//envoi des donn�es vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/joueurs/admin_joueurs_verifications.php",
					type   		: "POST",
					data     	: {lieu: lieu_ajoute},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_joueurs').modal('toggle');
								 },
					success  	: function(retour_json) 
								{  
									
									if(retour_json['resultat'] == tab_msg['code_ok']['id']) 
									{
									//champs valide
									$('#modal_ajout_joueurs_nom').removeClass('is-invalid');
									$('#modal_ajout_joueurs_nom').addClass('is-valid');
									$('#modal_ajout_joueurs_nom_label').addClass('text-success');
									$('#modal_ajout_joueurs_nom_aide').hide();
									
									$('#modal_ajout_joueurs_erreur').text("le lieu est valide");
									$('#modal_ajout_joueurs_button').attr('disabled',false);
									$('#modal_ajout_joueurs_button').removeClass('disabled');
									}
									else	
									{
										//le champs est d�j� utilis�
									$('#modal_ajout_joueurs_nom').addClass('is-invalid');
									$('#modal_ajout_joueurs_nom').removeClass('is-valid');
									$('#modal_ajout_joueurs_nom_label').addClass('text-danger');
									$('#modal_ajout_joueurs_nom_aide').hide();
									
									$('#modal_ajout_joueurs_erreur').text("le lieu est d�j� pr�sent");
									$('#modal_ajout_joueurs_button').attr('disabled',true);
									$('#modal_ajout_joueurs_button').addClass('disabled');
									}
									
								}							
						});
				}
				else
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_joueurs_nom').addClass('is-invalid');
				$('#modal_ajout_joueurs_nom_label').addClass('text-danger');
				$('#modal_ajout_joueurs_nom_aide').hide();
				
				$('#modal_ajout_joueurs_erreur').text("le nom doit comporter entre 4 et 20 lettres");					
				$('#modal_ajout_joueurs_button').attr('disabled',true);
				$('#modal_ajout_joueurs_button').addClass('disabled');
				}
			}	
			else		//le champs est vide
			{	
				// le champs est vide
				$('#modal_ajout_joueurs_nom').addClass('is-invalid');
				$('#modal_ajout_joueurs_nom_label').addClass('text-danger');
				$('#modal_ajout_joueurs_nom_aide').hide();
				
				$('#modal_ajout_joueurs_erreur').text("le  champs est vide");					
				$('#modal_ajout_joueurs_button').attr('disabled',true);
				$('#modal_ajout_joueurs_button').addClass('disabled');
			}					
		});
					
	
	$('#modal_ajout_joueurs_divers').blur(function() {
		
		var divers_ajoute = $('#modal_ajout_joueurs_divers').val();
		var longueur_divers_ajoute = divers_ajoute.length;
		//test du pattern
		if(pattern_joueurs_divers.test(divers_ajoute)&&(longueur_divers_ajoute<longueur_max_divers))
		{
		//champs valide
			$('#modal_ajout_joueurs_divers').removeClass('is-invalid');
			$('#modal_ajout_joueurs_divers').addClass('is-valid');
			$('#modal_ajout_joueurs_divers_label').removeClass('text-danger');
			$('#modal_ajout_joueurs_divers_label').addClass('text-success');
			
			$('#modal_ajout_joueurs_divers_erreur').text("le champs est valide!");
			$('#modal_ajout_joueurs_button').attr('disabled',false);
			$('#modal_ajout_joueurs_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_ajout_joueurs_divers').removeClass('is-valid');
			$('#modal_ajout_joueurs_divers').addClass('is-invalid');
			$('#modal_ajout_joueurs_divers_label').addClass('text-danger');
			$('#modal_ajout_joueurs_divers_label').removeClass('text-success');
			
			$('#modal_ajout_joueurs_divers_erreur').text("le champs doit comporter 255 caract�res maximum");					
			$('#modal_ajout_joueurs_button').attr('disabled',true);
			$('#modal_ajout_joueurs_button').addClass('disabled');
		}
	
	
	});
	
	
	$('#modal_ajout_joueurs_button').click(function(event){
		//r�cup�ration des informations de la fen�tre moddal
		var lieu_ajoute = $('#modal_ajout_joueurs_nom').val();
		var divers_ajoute = $('#modal_ajout_joueurs_divers').val();
		$('#ajout_joueurs_spinner').show();
		//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/joueurs/admin_joueurs_ajout.php",
			type   		: "POST",
			data     	: {lieu: lieu_ajoute,divers:divers_ajoute},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_joueurs').modal('toggle');
							$('#ajout_joueurs_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  
						$('#ajout_joueurs_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_ajout_joueurs').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_joueurs').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
});