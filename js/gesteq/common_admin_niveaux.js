	function initialisation_modal_ajout_lieux(fonction_callback)
	{
	$('#modal_ajout_lieux_nom').val("");
	$('#modal_ajout_lieux_divers').val("");
	$('#modal_ajout_lieux_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_lieux_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_lieux_nom_aide').show();
	$('#modal_ajout_lieux_button').attr('disabled',false);
	$('#modal_ajout_lieux_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}