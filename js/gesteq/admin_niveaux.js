$(function(){
var pattern_niveaux_nom = /.{3,}/i; 	//tous les caract�res nbre:3->20 sans saut ligne
var pattern_niveaux_divers = /.{0,255}/i;
var longueur_max_nom = 20;
var longueur_max_divers = 255;
	
var tab_msg={};

$('[data-toggle="tooltip"]').tooltip();

/********************************************************/
// fonctions admin niveaux
/********************************************************/
/**variagles globales                                  **/
function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise � jour du tableau des niveaux
			$.ajax({
				url      	: "code/niveaux/admin_niveaux_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								$('#nombre_niveaux').html("<b>"+reponse.length+"</b>");
								$('#table_niveaux tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_niveaux" name="modification_niveaux" 	data-toogle="tooltip" data-placement="top" title="modification du lieu"	value="modification_niveaux">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_niveaux" name="suppression_niveaux" title="suppression du lieu" data-toggle="tooltip" data-placement="top"   value="suppression_niveaux"><span class="fa fa-trash fa-1x "></span></button>';
								var ligne_table='';
								$.each(reponse,function(i,item){
								////console.log(item.resultat);
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id != 1) // on n'affiche pas la valeur par d�fut
										{
										ligne_table +='<tr class="admin_niveaux" id='+item.id+'><td>'+ item.nom + '</td><td>' + item.divers+ '</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
										}
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_niveaux').append(ligne_table);
							}						
					});	
			});
}
	
	$(document).ready(function(){
		//alert("mise � jour des variables de la page");
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		
		$('#suppression_niveaux_tous_spinner').hide();
		$('#ajout_niveaux_spinner').hide();
		
		mise_a_jour_liste();  


		//r�cup�ration des informations d'�changes du fichier JSON (code erreur,..)
		
		
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				
				////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
		
	});
	
	/*************************************************/
	// modal suppression de tous les niveaux en une fois
	/************************************************/	
	//affichage du modal
	$('#suppression_niveaux_tous').click(function(event){
		$('#modal_suppression_niveaux_tous').modal('show');
	});
	//appui sur le bouton de suppression
	$('#modal_suppression_niveaux_tous_button').click(function(event){
		$('#modal_suppression_niveaux_tous').hide();
		$('#suppression_niveaux_tous_spinner').show();
		
		//suppression en cours
		//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/niveaux/admin_niveaux_suppression_tous.php",
			type   		: "POST",		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_niveaux_tous').modal('toggle');
							$('#suppression_niveaux_tous_spinner').hide();
							mise_a_jour_liste();
							
						 },
			success  	: function(retour_json) 
						{  
						$('#suppression_niveaux_tous_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']==tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_niveaux_tous').modal('toggle');
							mise_a_jour_liste();
							
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_niveaux_tous').modal('toggle');
							
							mise_a_jour_liste();
							}
						}													
				});		
		
		
	});
	
	
	/*************************************************/
	// modal modification des niveaux
	/************************************************/
	
	//s�lection des boutons "modifier" par la m�thode de d�l�gation car ils sont ajout�s dynamiquement
	$('#table_niveaux').on("click",".modification_niveaux",function(e){
		
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		////console.log("id: "+id_recup +" / nom: "+nom_recup+" / valeur divers:"+divers_recup );
		
		//ouverture modal de modification
		$('#modal_modification_niveaux_nom').val("");
		$('#modal_modification_niveaux_divers').val("");
		$('#modal_modification_niveaux_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_niveaux_divers').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_niveaux_nom').focus();
		
		$('#modal_modification_niveaux').modal('show');
		$('#modal_modification_niveaux').on('shown.bs.modal', function() {
			$('#modal_modification_niveaux_nom').focus();
			$('#modal_modification_niveaux_nom').val(nom_recup);	
			$('#modal_modification_niveaux_divers').val(divers_recup);
			$('#modal_modification_niveaux_id').attr("value",id_recup);
			
			});	
		}); //fin du On click	
			
		//remise � z�ro des couleurs avant l'entr�e des informations	
		$( "#modal_modification_niveaux_nom" ).focus(function() {
					$('#modal_modification_niveaux_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_niveaux_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_niveaux_nom_aide').show();
					
					$('#modal_modification_niveaux_button').attr('disabled',false);
					$('#modal_modification_niveaux_button').removeClass('disabled');
		});
		
		//test de la validit� des donn�es
		$( "#modal_modification_niveaux_nom" ).blur(function() {
			
			//récupération de l'id de la ligne concern�e
			var id_lieu = $('#modal_modification_niveaux_id').attr("value");
			
		
			//recherche d'un �l�ment d�j� pr�sent
			var lieu_modifie = $('#modal_modification_niveaux_nom').val();
			var longueur_lieu_modifie = lieu_modifie.length;
			if($('#modal_modification_niveaux_nom').val()!="")
				{
				if(pattern_niveaux_nom.test(lieu_modifie)&&(longueur_lieu_modifie<longueur_max_nom))
				{
				//envoi des donn�es vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/niveaux/admin_niveaux_modification_verifications.php",
					type   		: "POST",
					data     	: {lieu: lieu_modifie, id:id_lieu},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_niveaux').modal('toggle');
								 },
					success  	: function(retour_json) {
								//3 cas: 	- le nom n'est pas pr�sent ->ok
								//			- le nom est pr�sent mais c'est celui du modal (on modifie le champs divers) ->ok
								//			- sinon, �chec
									////console.log("retour"+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										
										$('#modal_modification_niveaux_nom').addClass('is-valid');
										$('#modal_modification_niveaux_nom').removeClass('is-invalid');
										$('#modal_modification_niveaux_nom_label').addClass('text-success');
										$('#modal_modification_niveaux_erreur').text("le lieu est valide");
										$('#modal_modification_niveaux_button').attr('disabled',false);
										$('#modal_modification_niveaux_button').removeClass('disabled');
										$('#modal_modification_niveaux_nom_aide').hide();
									}
									else
									{
										$('#modal_modification_niveaux_nom').addClass('is-invalid');
										$('#modal_modification_niveaux_nom').removeClass('is-valid');
										$('#modal_modification_niveaux_nom_label').addClass('text-danger');
										$('#modal_modification_niveaux_nom_aide').hide();
										
										$('#modal_modification_niveaux_erreur').text("�chec");					
										$('#modal_modification_niveaux_button').attr('disabled',true);
										$('#modal_modification_niveaux_button').addClass('disabled');
									}
								}
					
					});
				}
				else
				{
				// le nom ne passe pas le pattern
				$('#modal_modification_niveaux_nom').addClass('is-invalid');
				$('#modal_modification_niveaux_nom_label').addClass('text-danger');
				$('#modal_modification_niveaux_nom_aide').hide();
				
				$('#modal_modification_niveaux_erreur').text("le nom doit comporter entre 4 et 20 caract�res");					
				$('#modal_modification_niveaux_button').attr('disabled',true);
				$('#modal_modification_niveaux_button').addClass('disabled');
				}
			}
		else
		{
		$('#modal_modification_niveaux_nom').addClass('is-invalid');
		$('#modal_modification_niveaux_nom_label').addClass('text-danger');
		$('#modal_modification_niveaux_nom_aide').hide();
		
		$('#modal_modification_niveaux_erreur').text("le  champs est vide");					
		$('#modal_modification_niveaux_button').attr('disabled',true);
		$('#modal_modification_niveaux_button').addClass('disabled');
		}
	});
		
	$('#modal_modification_niveaux_divers').blur(function() {
		
		var divers_modifie = $('#modal_modification_niveaux_divers').val();
		var longueur_divers_modifie = divers_modifie.length;
		//test du pattern
		if(pattern_niveaux_divers.test(divers_modifie)&&(longueur_divers_modifie<longueur_max_divers))
		{
		//champs valide
			$('#modal_modification_niveaux_divers').removeClass('is-invalid');
			$('#modal_modification_niveaux_divers').addClass('is-valid');
			$('#modal_modification_niveaux_divers_label').removeClass('text-danger');
			$('#modal_modification_niveaux_divers_label').addClass('text-success');
			
			$('#modal_modification_niveaux_divers_erreur').text("le champs est valide!");
			$('#modal_modification_niveaux_button').attr('disabled',false);
			$('#modal_modification_niveaux_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_niveaux_divers').removeClass('is-valid');
			$('#modal_modification_niveaux_divers').addClass('is-invalid');
			$('#modal_modification_niveaux_divers_label').addClass('text-danger');
			$('#modal_modification_niveaux_divers_label').removeClass('text-success');
		
			$('#modal_modification_niveaux_nom_aide').hide();
			$('#modal_modification_niveaux_divers_erreur').text("le champs doit comporter 255 caract�res maximum");					
			$('#modal_modification_niveaux_button').attr('disabled',true);
			$('#modal_modification_niveaux_button').addClass('disabled');
		}
	
	});
	
	//mise � jour des donn�es depuis la fen�tre modal
	$('#modal_modification_niveaux_button').click(function(event){
		
		//r�cup�ration des informations de la fen�tre moddal
		var lieu_modifie = $('#modal_modification_niveaux_nom').val();
		var divers_modifie = $('#modal_modification_niveaux_divers').val();
		var id_modifie = $('#modal_modification_niveaux_id').attr("value");
			//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/niveaux/admin_niveaux_modification.php",
			type   		: "POST",
			data     	: {lieu: lieu_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_niveaux').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_modification_niveaux').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_niveaux').modal('toggle');
							mise_a_jour_liste();
							
							}
						}													
				});
	});			
	/*************************************************/
	// modal suppression d'un lieu
	/************************************************/
	function initialisation_modal_suppresion_niveaux(fonction_callback)
	{
	$('#modal_suppression_niveaux_nom').val("");
	$('#modal_suppression_niveaux_divers').val("");
	$('#modal_suppression_niveaux_button').attr('disabled',false);
	$('#modal_suppression_niveaux_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	
	
	
	//s�lection des boutons "supprimer" par la m�thode de d�l�gation car ils sont ajout�s dynamiquement
	$('#table_niveaux').on("click",".suppression_niveaux",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		
	
		//ouverture modal de modification			
		initialisation_modal_ajout_niveaux(function(){
			$('#modal_suppression_niveaux_id').attr("value",id_recup);
			$('#modal_suppression_niveaux_nom ').text(nom_recup);	
			$('#modal_suppression_niveaux_divers').text(divers_recup);
			
			$('#modal_suppression_niveaux').modal('show'); 
			
			});
		}); //fin du On click	
		
		
			$('#modal_suppression_niveaux').on('shown.bs.modal', function() {		
			
			});			
		
		
	//mise � jour des donn�es depuis la fen�tre modal
	$('#modal_suppression_niveaux_button').click(function(event){
		
		//r�cup�ration des informations de la fen�tre moddal
		var lieu_modifie = $('#modal_suppression_niveaux_nom').text();
		var divers_modifie = $('#modal_suppression_niveaux_divers').text();
		var id_modifie = $('#modal_suppression_niveaux_id').attr("value");
		
		//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/niveaux/admin_niveaux_suppression.php",
			type   		: "POST",
			data     	: {lieu: lieu_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_niveaux').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_niveaux').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_niveaux').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});			
	
	/*************************************************/
	// modal ajout des niveaux
	/************************************************/
/*	
	function initialisation_modal_ajout_niveaux(fonction_callback)
	{
	$('#modal_ajout_niveaux_nom').val("");
	$('#modal_ajout_niveaux_divers').val("");
	$('#modal_ajout_niveaux_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_niveaux_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_niveaux_nom_aide').show();
	$('#modal_ajout_niveaux_button').attr('disabled',false);
	$('#modal_ajout_niveaux_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	 
*/	
	
	$('#ajout_niveaux').click(function() {
		initialisation_modal_ajout_niveaux(function(){
			$('#modal_ajout_niveaux').modal('show');
			$('#modal_ajout_niveaux_nom').focus()	
			});
		});
		
		$('#modal_ajout_niveaux').on('shown.bs.modal', function() {
			$('#modal_ajout_niveaux_nom').focus();
						});
		
		$('#modal_ajout_niveaux').on('hidden.bs.modal', function (e) {
//en r�serve		
		});
			
		
		//remise � z�ro des couleurs avant l'entr�e des informations	
		$( "#modal_ajout_niveaux_nom" ).focus(function() {
					$('#modal_ajout_niveaux_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_niveaux_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_niveaux_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_niveaux_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_niveaux_nom_aide').show();
					$('#modal_ajout_niveaux_button').attr('disabled',false);
					$('#modal_ajout_niveaux_button').removeClass('disabled');
				
			});
		
		//test de la validit� des donn�es
		$( "#modal_ajout_niveaux_nom" ).blur(function() {
			
			//recherche d'un �l�ment d�j� pr�sent
			var lieu_ajoute = $('#modal_ajout_niveaux_nom').val();
			var longueur_lieu_ajoute = lieu_ajoute.length;
			
			
			if(($('#modal_ajout_niveaux_nom').val()!=""))
			{	
				//test du pattern
				if(pattern_niveaux_nom.test(lieu_ajoute)&&(longueur_lieu_ajoute<longueur_max_nom))
				{
				//envoi des donn�es vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/niveaux/admin_niveaux_verifications.php",
					type   		: "POST",
					data     	: {lieu: lieu_ajoute},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_niveaux').modal('toggle');
								 },
					success  	: function(retour_json) 
								{  
									
									if(retour_json['resultat'] == tab_msg['code_ok']['id']) 
									{
									//champs valide
									$('#modal_ajout_niveaux_nom').removeClass('is-invalid');
									$('#modal_ajout_niveaux_nom').addClass('is-valid');
									$('#modal_ajout_niveaux_nom_label').addClass('text-success');
									$('#modal_ajout_niveaux_nom_aide').hide();
									
									$('#modal_ajout_niveaux_erreur').text("le lieu est valide");
									$('#modal_ajout_niveaux_button').attr('disabled',false);
									$('#modal_ajout_niveaux_button').removeClass('disabled');
									}
									else	
									{
										//le champs est d�j� utilis�
									$('#modal_ajout_niveaux_nom').addClass('is-invalid');
									$('#modal_ajout_niveaux_nom').removeClass('is-valid');
									$('#modal_ajout_niveaux_nom_label').addClass('text-danger');
									$('#modal_ajout_niveaux_nom_aide').hide();
									
									$('#modal_ajout_niveaux_erreur').text("le lieu est d�j� pr�sent");
									$('#modal_ajout_niveaux_button').attr('disabled',true);
									$('#modal_ajout_niveaux_button').addClass('disabled');
									}
									
								}							
						});
				}
				else
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_niveaux_nom').addClass('is-invalid');
				$('#modal_ajout_niveaux_nom_label').addClass('text-danger');
				$('#modal_ajout_niveaux_nom_aide').hide();
				
				$('#modal_ajout_niveaux_erreur').text("le nom doit comporter entre 4 et 20 lettres");					
				$('#modal_ajout_niveaux_button').attr('disabled',true);
				$('#modal_ajout_niveaux_button').addClass('disabled');
				}
			}	
			else		//le champs est vide
			{	
				// le champs est vide
				$('#modal_ajout_niveaux_nom').addClass('is-invalid');
				$('#modal_ajout_niveaux_nom_label').addClass('text-danger');
				$('#modal_ajout_niveaux_nom_aide').hide();
				
				$('#modal_ajout_niveaux_erreur').text("le  champs est vide");					
				$('#modal_ajout_niveaux_button').attr('disabled',true);
				$('#modal_ajout_niveaux_button').addClass('disabled');
			}					
		});
					
	
	$('#modal_ajout_niveaux_divers').blur(function() {
		
		var divers_ajoute = $('#modal_ajout_niveaux_divers').val();
		var longueur_divers_ajoute = divers_ajoute.length;
		//test du pattern
		if(pattern_niveaux_divers.test(divers_ajoute)&&(longueur_divers_ajoute<longueur_max_divers))
		{
		//champs valide
			$('#modal_ajout_niveaux_divers').removeClass('is-invalid');
			$('#modal_ajout_niveaux_divers').addClass('is-valid');
			$('#modal_ajout_niveaux_divers_label').removeClass('text-danger');
			$('#modal_ajout_niveaux_divers_label').addClass('text-success');
			
			$('#modal_ajout_niveaux_divers_erreur').text("le champs est valide!");
			$('#modal_ajout_niveaux_button').attr('disabled',false);
			$('#modal_ajout_niveaux_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_ajout_niveaux_divers').removeClass('is-valid');
			$('#modal_ajout_niveaux_divers').addClass('is-invalid');
			$('#modal_ajout_niveaux_divers_label').addClass('text-danger');
			$('#modal_ajout_niveaux_divers_label').removeClass('text-success');
			
			$('#modal_ajout_niveaux_divers_erreur').text("le champs doit comporter 255 caract�res maximum");					
			$('#modal_ajout_niveaux_button').attr('disabled',true);
			$('#modal_ajout_niveaux_button').addClass('disabled');
		}
	
	
	});
	
	
	$('#modal_ajout_niveaux_button').click(function(event){
		//r�cup�ration des informations de la fen�tre moddal
		var lieu_ajoute = $('#modal_ajout_niveaux_nom').val();
		var divers_ajoute = $('#modal_ajout_niveaux_divers').val();
		$('#ajout_niveaux_spinner').show();
		//envoi des donn�es vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/niveaux/admin_niveaux_ajout.php",
			type   		: "POST",
			data     	: {lieu: lieu_ajoute,divers:divers_ajoute},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_niveaux').modal('toggle');
							$('#ajout_niveaux_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  
						$('#ajout_niveaux_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_ajout_niveaux').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_niveaux').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
});