var tab_provisoire={};
var tab_provisoire_initiale={};
var tab_msg={};
var tab_etat_initial_joueur=[];
function acqusition_echange()
{
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				});
}
/************************************************************************************************************/
//	chargement de la page
// 			chargement des donnees
/************************************************************************************************************/
		$(function(){
			//masque des toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			$('#table_grille_presence_entete').empty();
			$('#table_grille_presence_ligne').empty();
			//masque des spinners
			$.ajax({
				url		:"./constantes/code_message.json",
				Type	:"GET",
				cache	:false,
				dataType:"json",
				error		: function(request,error){
											alert("Erreur : responseText: "+request.responseText);
											},
				success		:function(data)
							{
							var tab_msg={};
							$.each(data,function(index,d){			
								var tab_msg_tampon={};
								tab_msg_tampon['id']= d.id;
								tab_msg_tampon['nom']=d.nom;
								tab_msg_tampon['texte']=d.texte;
								tab_msg[index]=tab_msg_tampon;
								});
							////console.log(tab_msg);
							var ligne_select='';
							var ligne_form='';
							var index_tab=01;
							//récupration de la liste des états
								$.ajax({
											url			:"./code/admin_etats/admin_liste_etats.php", 
											type		:"POST",
											cache		:false,
											dataType	:"json",
											error		: function(request,error){
														alert("Erreur : responseText: "+request.responseText);
														},
											success		: function(reponse)
														{
															ligne_form+='<select  class="custom-select select_etat_joueur">\
																			<option value="-1">   </option>'
															$.each(reponse,function(i,item){
															////console.log(item.resultat);
															if(item.resultat ==  tab_msg['code_ok']['id'])
															{
																ligne_form +='<option  value='+item.id+'> '+item.nom+' </option>';
															}
															else
															{
																alert("erreur");
																var res = item.resultat;
																var message = tab_msg[res]['texte'];
																$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																$('#toast_enregistrement_echec').toast('show');
															}
															});
															ligne_form+='</select>';
															$.ajax({
																	url			:"./code/composition_equipe/liste_date.php", 
																	type		:"POST",
																	cache		:false,
																	dataType	:"json",
																	error		: function(request,error){
																				alert("Erreur : responseText: "+request.responseText);
																				},
																	success		: function(reponse)
																				{
																					var ligne_entete='';
																					ligne_entete+='<th col=\"scope\">joueur  </th>'
																					$.each(reponse,function(i,item){
																					////console.log(item.resultat);
																					if(item.resultat ==  tab_msg['code_ok']['id'])
																					{
																						ligne_entete +='<th col=\"scope\" id='+item.id+'> J'+item.journee+' / '+item.date+'</th>';
																						ligne_select +='<td id='+index_tab+'>'+ligne_form+'</td>';
																						index_tab++;
																					}
																					else
																					{
																						alert("erreur");
																						var res = item.resultat;
																						var message = tab_msg[res]['texte'];
																						$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																						$('#toast_enregistrement_echec').toast('show');
																					}
																					});
																					$('#table_grille_presence_entete').append(ligne_entete);
																						//chargment des joueurs
																						$.ajax({
																							url			:"code/grille_presence/liste_joueurs.php", 
																							type		:"POST",
																							cache		:false,
																							dataType	:"json",
																							error		: function(request,error){
																										alert("Erreur : responseText: "+request.responseText);
																										},
																							success		: function(reponse)
																										{
																											console.log(reponse);
																											$('#table_grille_presence_ligne tr').each(function(){
																												$(this).remove();
																											});
																											var ligne='';
																											var button_select;
																											$.each(reponse,function(i,item){
																											if(item.resultat ==  tab_msg['code_ok']['id'])
																											{
																												ligne+='<tr class="grille_presence_ligne" id='+item.id+'><td>'+ item.nom + ' ' + item.prenom+'\
																												<span class="badge badge-primary badge-pill">'+item.classement+'</span>\
																												</td>'+ligne_select+'</tr>';
																											}
																											else
																											{
																												alert("erreur");
																												var res = item.resultat;
																												var message = tab_msg[res]['texte'];
																												$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																												$('#toast_enregistrement_echec').toast('show');
																											}
																											});
																											$('#table_grille_presence_ligne').append(ligne);
																												$.ajax({
																													url			:"./code/grille_presence/liste_etats_joueurs.php", 
																													type		:"POST",
																													cache		:false,
																													dataType	:"json",
																													error		: function(request,error){
																																alert("Erreur : responseText: "+request.responseText);
																																},
																													success		: function(reponse)
																																{
																																console.log(reponse);
																																$("select").each(function(i,index){
																																	console.log("L:"+$(this).parent().prop("id")+"/C: "+$(this).parent().parent().prop("id"));
																																	var ligne = $(this).parent().parent().prop("id");
																																	var colonne = $(this).parent().prop("id");
																																	$(this).find('option[value="'+reponse[i]['etat']+'"]').prop('selected', true);
																																	});
																																}
																												});
																										}
																							});
																				}
																});
														}
									});			
							}
				});
				acqusition_echange();
//affichage des valeurs initiales
		});
//récupération de la valeur de select sur un changmement
$('#table_grille_presence').on("change",".select_etat_joueur",function(e){
		acqusition_echange();
		var id_etat_selectionne = $(e.currentTarget).val();
		var id_colonne = $(e.currentTarget).parent().attr("id");
		var id_ligne = $(e.currentTarget).closest('tr').attr("id");
		//console.log("id colonne (date):"+id_colonne);
		//console.log("id ligne (joueur):"+id_ligne);
		//console.log("id etat retenu :"+id_etat_selectionne);
								//mise à jour de l'état du joueur dans la base immédiatement
								$.ajax({
										url			:"./code/grille_presence/update_etats_joueurs.php", 
										type		:"POST",
										cache		:false,
										data		:{id_date_selectionne:id_colonne, id_joueur_selectionne:id_ligne, id_etat_initial_selectionne:id_etat_selectionne},
										dataType	:"json",
										error		: function(request,error){
													alert("Erreur : responseText: "+request.responseText);
													},
										success		: function(reponse)
													{
														console.log(reponse);
														if(reponse.resultat ==  tab_msg['code_ok']['id'])
														{
														}
														else
														{
															var res = reponse.resultat;
															var message = tab_msg[res]['texte'];
															$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
															$('#toast_enregistrement_echec').toast('show');
														}
													}
								});
	});