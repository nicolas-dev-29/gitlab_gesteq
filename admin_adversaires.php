<!--		grille des présences et absences des joueurs		
				date:01/07/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/gesteq_constante.inc");
	//include ("./code/modal_lieux.php");
	//include ("./code/etat/modal_etat.php");
	include ("./code/toast_perso.php");
?>
<div class="container-fluid ">
	<div class="row">
		<div class="col-lg-12">
			<h1> Grille des adversaires </h1>
		</div>
	</div>
	<div class="row ">
		<div class=" offset-lg-1 col-lg-10 col-sm-12 ">
			<div class="table-responsive ">
				<table class="table   align-middle text-center table-condensed table-stripped my-3" id="table_grille_adversaires">
					<thead id="table_grille_adversaires_entete">
						<!-- insertion des données par jquery depuis une requête AJAX -->
					</thead>
					<tbody id="table_grille_adversaires_ligne">
						
						<!-- insertion des données par jquery depuis une requête AJAX -->
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="js/gesteq/admin_adversaires.js"></script>
<!--<script src="js/gesteq/essai_ajax.js"></script>-->